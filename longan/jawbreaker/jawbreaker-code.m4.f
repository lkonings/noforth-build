hex

create PATTERNS ( -- a )    \ All eight possible RGB led patterns
    2006 h, 2004 h, 2002 h, 2000 h, 0006 h,  0004 h,   0002 h, 0000 h,  

: !LEDS         ( +n -- )
    7 and  2* patterns +  h@ >r   \ Get pattern
    r@ 6 and __GPIOA_OCTL h!      \ Handle porta
    r> 2000 and __GPIOC_OCTL h! ; \ Handle portc
    

: JAWBREAKER    ( -- )       \ Dutch: Toverbal
    0006 __GPIOA_OCTL **bic  \ Switch off leds which are active low
    2000 __GPIOC_OCTL **bic
    44444224 __GPIOA_CTL0 !  \ Set pa1 and pa2 as output (Reset $44444444)
    44244444 __GPIOC_CTL1 !  \ Set pc13 as output (Reset $44444444)
    begin
        8 for  i !leds  100 ms  next \ Output all eight patterns once
    key? until               \ Loop until reset or keyboard key pressed
    0 !leds ;                \ Leds off

' jawbreaker  to app  freeze

\ End ;;;
