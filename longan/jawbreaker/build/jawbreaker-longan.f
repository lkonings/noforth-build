(* Short demo to Flash an onboard RGB LED on Longan Nano, inspired by the Mecrisp blinky

Values for CRL/CRH-Registers:
 0:  Analog Input
 1:  Output Push/Pull, 10 MHz
 2:  Output Push/Pull,  2 MHz
 3:  Output Push/Pull, 50 MHz
 4:  Floating Input (Reset state)
 5:  Open-Drain Output, 10 MHz
 6:  Open-Drain Output,  2 MHz
 7:  Open-Drain Output, 50 MHz
 8:  Input with pull-up / pull-down
 9:  Alternate Function, Push/Pull, 10 MHz
 A:  Alternate Function, Push/Pull,  2 MHz
 B:  Alternate Function, Push/Pull, 50 MHz
 C:  Reserved
 D:  Alternate Function, Open-Drain, 10 MHz
 E:  Alternate Function, Open-Drain,  2 MHz
 F:  Alternate Function, Open-Drain, 50 MHz

*)

40010800 constant __GPIOA_CTL0
4001080C constant __GPIOA_OCTL
40011004 constant __GPIOC_CTL1
4001100C constant __GPIOC_OCTL

hex

create PATTERNS ( -- a )              \ All eight possible RGB led patterns
    2006 h, 2004 h, 2002 h, 2000 h, 0006 h,  0004 h,   0002 h, 0000 h,  

: !LEDS         ( +n -- )
    7 and  2* patterns +  h@ >r       \ Get pattern
    r@ 6 and __GPIOA_OCTL h!          \ Handle porta
    r> 2000 and __GPIOC_OCTL h! ;     \ Handle portc
    

: JAWBREAKER    ( -- )                \ Dutch: Toverbal
    0006 __GPIOA_OCTL **bic           \ Switch off leds which are active low
    2000 __GPIOC_OCTL **bic
    44444224 __GPIOA_CTL0 !           \ Set pa1 and pa2 as output (Reset $44444444)
    44244444 __GPIOC_CTL1 !           \ Set pc13 as output (Reset $44444444)
    begin
        8 for  i !leds  100 ms  next  \ Output all eight patterns once
    key? until                        \ Loop until reset or keyboard key pressed
    0 !leds ;                         \ Leds off

' jawbreaker  to app  freeze

\ End ;;;
