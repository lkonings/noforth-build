

(* Sleeping Launchpad, Egel kit or Micro Launchpad using the VLO & timer-A0

  Register addresses for Timer-A
160 - TA0CTL   Timer A control
162 - TA0CCTL0 Timer A Comp/Capt. control 0
172 - TACCR0   Timer A Compare register

  Used MSP430 power modes name shortcuts & short explanation:
  AM   = Active Mode      - CPU & clocks active
  LPM3 = Low Power Mode 3 - Only ACLK active

*)

value PERIOD                                 \ Decreases almost every 4.5 seconds
code LPM3       D8 # sr bis  next  end-code  \ Go from AM to LPM3

\ Set timer compare interrupt on with ACLK and init. leds
\ Clock divider=/1, Up mode & Timer A clear.
: ACLK-ON       ( -- )
    01 022 *bis                              \ P1DIR    Set P1.0 with LED to output
    01 021 *bic                              \ P1OUT    Set led off
    124 160 !                                \ TA0CTL   Set timer mode to ACLK/2
    20 53 c!                                 \ BCSCTL3  12kHz VLOCLK on 
    10 162 ! ;                               \ TA0CCTL0 Enable interrupts on Compare 0

: PREPARE     ( -- )  01 022 *bis ;          \ P1.0=Red led

: SLEEP       ( u -- )                       \ Maximal time = Very long, 0 = 4.5 seconds
    to period  lpm3 ;                        \ Sleep until time has passed

\ Decrease TIME) until it's zero
code TIMER      ( -- )
    #0 adr period & cmp
    =? if,
        F8 # rp ) bic                        \ Interrupt off, CPU active again!
        reti
    then,
    #-1 adr period & add
    reti
end-code
' timer >body FFF2 vec!                      \ Install timer-A0 interrupt vector

: LED-ON    ( -- )      01 021 *bis ;
: LED-OFF   ( -- )      01 021 *bic ;
: FLASH     ( -- )      led-on FF ms  led-off FF ms ;

: MILL      ( n -- )                         \ Funny windmill
    0 ?do
      ch / emit 80 ms  8 emit  led-on
      ch - emit 80 ms  8 emit  led-off
      ch                                     \ emit 80 ms  8 emit  led-on
      ch | emit 80 ms  8 emit  led-off
      s? 0= if  quit  then                   \ Quit on S2 keypress
    loop ;

\ Flashing windmill with 9 seconds LMP3 intervals
\ Until S2 is pressed while doing the windmill
: INTERVAL  ( -- )
    prepare aclk-on  flash  begin  cr ." CPU Active "  5 mill  1 sleep  again ;


' interval  to app
shield LPM3\  freeze

\ End ;;;
