(* E04 - For noForth C&V 200202: Port output with MSP430G2553 at port-2.

  RS232 via USB or Bluetooth in- and output met, with a copy at the LEDS.
  Use Bluetooth instead of USB serial connection... for MSP430G2553 version-A.
  Change KEY to show characters from Bluetooth or USB at the LEDS, the
  Launchpad has only uart0, so no fancy tricks here. Use the HC06 bluetooth
  module here only four wires need to be connected to the module.

  For Bluetooth two jumpers need to be removed, the TX and RX jumpers at J3
  Connect the power for the HC06 {pin12+13} with Launchpad J6 {VCC+GND},
  TX & RX from HC06 {pin1+2} with Launchpad J1 {pin3+4}

  Address 029 - P2OUT, port-2 output with 8 leds
  Address 02A - P2DIR, port-2 direction register
  Address 02E - P2SEL, port-2 selction register
  *)

