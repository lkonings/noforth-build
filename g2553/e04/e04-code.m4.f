hex
\ This key shows also the binary character pattern on the leds
: >LEDS         ( u -- )    __P2OUT_A c! ; \ P2OUT  Store pattern u at leds
: KEY*          ( c -- )    key)  dup >leds ;

: FLASH         ( -- )          \ Visualise startup
    FF >leds  64 ms             \ All leds on
    00 >leds  64 ms ;           \ All leds off

: SETUP         ( -- )
    0 __P2SEL_A c!             \ P2SEL  Port-2 all bits I/O
    -1 __P2DIR_A c! ;          \ P2DIR  All bits of P2 are outputs

: STARTUP       ( -- )
    setup  flash                \ Show new boot sequence
    ['] key* to 'key ;          \ Install new KEY

' startup to app  freeze

\ End ;;;
