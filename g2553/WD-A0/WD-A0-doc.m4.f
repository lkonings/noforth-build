(* Sleeping Launchpad, Egel kit or Micro Launchpad using the VLO & timer-A0

   Code used from E24 - For noForth C&V2553 lp.0, Watchdog initialisation and use.

   The primary function of a watchdog timer, is to perform a controlled 
   system restart after a software problem occurs.

   Used hardware registers:
   0001 0002 - IFG1   Interrupt flag register 1
   xxxx 0120 - WDTCTL Watchdog control register

   The lowest two bits of the WDTCL register set the clock divider.
   In this example it is set to 00 = SMCLK/32768. The activating time 
   of the watchdog may be changed. This may be done with an other clock 
  (ACLK or SMCLK) and/or an other clock divider.
   
   When the value 'U > 0780' the watchdog restarts noForth.
   The user word is: WATCHDOG  ( u -- )

   The settings for the watchdog can be found from page 346
   and and beyond in SLAU144J.PDF 

  ---- TIMER ----

  Register addresses for Timer-A
  160 - TA0CTL   Timer A control
  162 - TA0CCTL0 Timer A Comp/Capt. control 0
  172 - TACCR0   Timer A Compare register

  Used MSP430 power modes name shortcuts & short explanation:
  AM   = Active Mode      - CPU & clocks active
  LPM3 = Low Power Mode 3 - Only ACLK active

*)

