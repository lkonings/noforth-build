

(* Sleeping Launchpad, Egel kit or Micro Launchpad using the VLO & timer-A0

   Code used from E24 - For noForth C&V2553 lp.0, Watchdog initialisation and use.

   The primary function of a watchdog timer, is to perform a controlled 
   system restart after a software problem occurs.

   Used hardware registers:
   0001 0002 - IFG1   Interrupt flag register 1
   xxxx 0120 - WDTCTL Watchdog control register

   The lowest two bits of the WDTCL register set the clock divider.
   In this example it is set to 00 = SMCLK/32768. The activating time 
   of the watchdog may be changed. This may be done with an other clock 
  (ACLK or SMCLK) and/or an other clock divider.
   
   When the value 'U > 0780' the watchdog restarts noForth.
   The user word is: WATCHDOG  ( u -- )

   The settings for the watchdog can be found from page 346
   and and beyond in SLAU144J.PDF 

  ---- TIMER ----

  Register addresses for Timer-A
  160 - TA0CTL   Timer A control
  162 - TA0CCTL0 Timer A Comp/Capt. control 0
  172 - TACCR0   Timer A Compare register

  Used MSP430 power modes name shortcuts & short explanation:
  AM   = Active Mode      - CPU & clocks active
  LPM3 = Low Power Mode 3 - Only ACLK active

*)

\ WATCHDOG

\ Show watchdog reset from IFG1 register
\ IFG1, Bit 0 = Watchdog interrupt flag
: .(RE)START    ( -- )
    cr ." Restart noForth " 
    01 002 bit* if  ." by watchdog timer "  then  \ IFG1 
    01 002 *bic                                   \ IFG1  Reset watchdog interrupt flag
;

: WD-ON         ( -- )    5A08 120 ! ;            \ WDTCTL - (Re)activate watchdog timer
: WD-OFF        ( -- )    5A80 120 ! ;            \ WDTCTL - Watchdog deactivated

\ RANDOM

value RND   ( Work location for pseudo random numbers )

decimal     ( Make pseudo random number ud )
: RANDOM        ( -- ud )       rnd 31421 *  6927 +  dup to rnd ;
: CHOOSE        ( u1 -- u2 )    random  um* nip ;
: SETUP-RANDOM  ( -- )          31414 to rnd ;
hex

\ TIMER

value PERIOD                                      \ Decreases almost every 4.5 seconds
code LPM3       D8 # sr bis  next  end-code       \ Go from AM to LPM3

\ Set timer compare interrupt on with ACLK and init. leds
\ Clock divider=/1, Up mode & Timer A clear.
: ACLK-ON       ( -- )
    01 022 *bis                                   \ P1DIR    Set P1.0 with LED to output
    01 021 *bic                                   \ P1OUT    Set led off
    124 160 !                                     \ TA0CTL   Set timer mode to ACLK/2
    20 53 c!                                      \ BCSCTL3  12kHz VLOCLK on 
    10 162 ! ;                                    \ TA0CCTL0 Enable interrupts on Compare 0

: PREPARE     ( -- )  01 022 *bis ;               \ P1.0=Red led

: SLEEP       ( u -- )                            \ Maximal time = Very long, 0 = 4.5 seconds
    to period  lpm3 ;                             \ Sleep until time has passed

\ Decrease TIME) until it's zero
code TIMER      ( -- )
    #0 adr period & cmp
    =? if,
        F8 # rp ) bic                             \ Interrupt off, CPU active again!
        reti
    then,
    #-1 adr period & add
    reti
end-code
' timer >body FFF2 vec!                           \ Install timer-A0 interrupt vector

: LED-ON    ( -- )      01 021 *bis ;
: LED-OFF   ( -- )      01 021 *bic ;
: FLASH     ( -- )      led-on FF ms  led-off FF ms ;

: MIGHT-HANG ( perc -- )
  dm 100 choose swap <
  if
    cr ." HANGING..."
    begin again
  then ;

: MILL      ( n -- )                              \ Funny windmill
    wd-on                                         \ (Re)start watchdog
    cr
    0 ?do
      ch / emit 80 ms  8 emit  led-on
      ch - emit 80 ms  8 emit  led-off
      ch \ emit 80 ms  8 emit  led-on
      ch | emit 80 ms  8 emit  led-off
                                                  \ 33 might-hang    
      s? 0= if  quit  then                        \ Quit on S2 keypress
    loop
                                                  \ wd-off
 ;


\ Flashing windmill with 9 seconds LMP3 intervals
\ Until S2 is pressed while doing the windmill
: INTERVAL  ( -- )
    .(re)start
    setup-random
    prepare aclk-on  flash
    begin
                                                  \ cr ." CPU Active "  
      5 mill  1 sleep  
    again ;

' interval  to app
shield LPM3\  freeze

\ End ;;;
