
100 constant __SFRIE1_A
102 constant __SFRIE1_A
104 constant __SFRRPCR_A

120 constant __PMMCTL0_A
12A constant __PMMFG_A
130 constant __PM5CTL0_A

140 constant __FRCTL0_A
144 constant __GCCCTL0_A
146 constant __GCCCTL1_A

150 constant __CRC16DI_A
152 constant __CRCDIRB_A
154 constant __CRCINIRES_A
156 constant __CRCRESR_A

158 constant __RCCTL0_A

15C constant __WDTCTL_A

160 constant __CSCTL0_A
162 constant __CSCTL1_A
164 constant __CSCTL2_A
166 constant __CSCTL3_A
168 constant __CSCTL4_A
16A constant __CSCTL5_A
16C constant __CSCTL6_A

180 constant __SYSCTL_A
186 constant __SYSJMBC_A
188 constant __SYSJMBI0_A
18A constant __SYSJMBI1_A
18C constant __SYSJMBO0_A
18E constant __SYSJMBO1_A
19A constant __SYSUNIV_A
19C constant __SYSRSIV_A
19E constant __SYSRSTIV_A

1B0 constant __REFCTL_A

200 constant __P1
200 constant __P1IN_A
202 constant __P1OUT_A
204 constant __P1DIR_A
206 constant __P1REN_A
20A constant __P1SEL0_A
20C constant __P1SEL1_A
20E constant __P1IV_A
210 constant __P1SELC_A
218 constant __P1IES_A
21A constant __P1IE_A
21C constant __P1IFG_A

201 constant __P2
201 constant __P2IN_A
203 constant __P2OUT_A
205 constant __P2DIR_A
207 constant __P2REN_A
20B constant __P2SEL0_A
20D constant __P2SEL1_A
21E constant __P2IV_A
211 constant __P2SELC_A
219 constant __P2IES_A
21B constant __P2IE_A
21D constant __P2IFG_A

220 constant __P3
220 constant __P3IN_A
222 constant __P3OUT_A
224 constant __P3DIR_A
226 constant __P3REN_A
22A constant __P3SEL0_A
22C constant __P3SEL1_A
22E constant __P3IV_A
230 constant __P3SELC_A
238 constant __P3IES_A
23A constant __P3IE_A
23C constant __P3IFG_A

221 constant __P4
221 constant __P4IN_A
223 constant __P4OUT_A
225 constant __P4DIR_A
227 constant __P4REN_A
22B constant __P4SEL0_A
22D constant __P4SEL1_A
22E constant __P4IV_A
231 constant __P4SELC_A
239 constant __P4IES_A
23B constant __P4IE_A
23D constant __P4IFG_A

240 constant __P5
240 constant __P5IN_A
242 constant __P5OUT_A
244 constant __P5DIR_A
246 constant __P5REN_A
24A constant __P5SEL0_A
24C constant __P5SEL1_A
24E constant __P5IV_A
250 constant __P5SELC_A
258 constant __P5IES_A
25A constant __P5IE_A
25C constant __P5IFG_A

241 constant __P6
241 constant __P6IN_A
243 constant __P6OUT_A
245 constant __P6DIR_A
247 constant __P6REN_A
24B constant __P6SEL0_A
24D constant __P6SEL1_A
24E constant __P6IV_A
251 constant __P6SELC_A
259 constant __P6IES_A
25B constant __P6IE_A
25D constant __P6IFG_A

260 constant __P7
260 constant __P7IN_A
262 constant __P7OUT_A
264 constant __P7DIR_A
266 constant __P7REN_A
26A constant __P7SEL0_A
26C constant __P7SEL1_A
26E constant __P7IV_A
260 constant __P7SELC_A
268 constant __P7IES_A
26A constant __P7IE_A
26C constant __P7IFG_A

261 constant __P8
261 constant __P8IN_A
263 constant __P8OUT_A
265 constant __P8DIR_A
267 constant __P8REN_A
26B constant __P8SEL0_A
26D constant __P8SEL1_A
26E constant __P8IV_A
271 constant __P8SELC_A
279 constant __P8IES_A
27B constant __P8IE_A
27D constant __P8IFG_A

320 constant __PJ
320 constant __PJIN_A
322 constant __PJOUT_A
324 constant __PJDIR_A
326 constant __PJREN_A
32A constant __PJSEL0_A
32C constant __PJSEL1_A
336 constant __PJSELC_A

340 constant __TA0CTL_A
342 constant __TA0CTL0_A
344 constant __TA0CTL1_A
346 constant __TA0CTL2_A
350 constant __TA0R_A
352 constant __TA0CCR0_A
354 constant __TA0CCR1_A
356 constant __TA0CCR2_A
360 constant __TA0EX0_A
36E constant __TA0IV_A

380 constant __TA1CTL_A
382 constant __TA1CTL0_A
384 constant __TA1CTL1_A
386 constant __TA1CTL2_A
390 constant __TA1R_A
392 constant __TA1CCR0_A
394 constant __TA1CCR1_A
396 constant __TA1CCR2_A
3A0 constant __TA1EX0_A
3AE constant __TA1IV_A

3C0 constant __TB0CTL_A
3C2 constant __TB0CTL0_A
3C4 constant __TB0CTL1_A
3C6 constant __TB0CTL2_A
3C8 constant __TB0CTL3_A
3CA constant __TB0CTL4_A
3CC constant __TB0CTL5_A
3CE constant __TB0CTL6_A
3D0 constant __TB0R_A
3D2 constant __TB0CCR0_A
3D4 constant __TB0CCR1_A
3D6 constant __TB0CCR2_A
3D8 constant __TB0CCR3_A
3DA constant __TB0CCR4_A
3DC constant __TB0CCR5_A
3DE constant __TB0CCR6_A
3E0 constant __TB0EX0_A
3EE constant __TB00IV_A

400 constant __TA2CTL_A
402 constant __TA2CTL0_A
404 constant __TA2CTL1_A
406 constant __TA2CTL2_A
410 constant __TA2R_A
412 constant __TA2CCR0_A
414 constant __TA2CCR1_A
416 constant __TA2CCR2_A
420 constant __TA2EX0_A
42E constant __TA2IV_A

43E constant __CAPTIO0CTL_A

440 constant __TA3CTL_A
442 constant __TA3CTL0_A
444 constant __TA3CTL1_A
446 constant __TA3CTL2_A
450 constant __TA3R_A
452 constant __TA3CCR0_A
454 constant __TA3CCR1_A
456 constant __TA3CCR2_A
460 constant __TA3EX0_A
46E constant __TA3IV_A

5C0 constant __UCA0CTLW0_A
5C2 constant __UCA0CTLW1_A
5C6 constant __UCA0BRW_A
5C6 constant __UCA0BR0_A
5C7 constant __UCA0BR1_A
5C8 constant __UCA0MCTLW_A
5CA constant __UCA0STATW_A
5CC constant __UCA0RXBUF_A
5CE constant __UCA0TXBUF_A
5D0 constant __UCA0ABCCTL_A
5D2 constant __UCA0IRTCTL_A
5D3 constant __UCA0IRRCTL_A
5DA constant __UCA0IFG_A
5DC constant __UCA0IFG_A
5DE constant __UCA0IV_A

5E0 constant __UCA1CTLW0_A
5E2 constant __UCA1CTLW1_A
5E6 constant __UCA1BRW_A
5E6 constant __UCA1BR0_A
5E7 constant __UCA1BR1_A
5E8 constant __UCA1MCTLW_A
5EA constant __UCA1STATW_A
5EC constant __UCA1RXBUF_A
5EE constant __UCA1TXBUF_A
5F0 constant __UCA1ABCCTL_A
5F2 constant __UCA1IRTCTL_A
5F3 constant __UCA1IRRCTL_A
5FA constant __UCA1IFG_A
5FC constant __UCA1IFG_A
5FE constant __UCA1IV_A

606 constant __UCA2BRW_A
606 constant __UCA2BR0_A
607 constant __UCA2BR1_A
608 constant __UCA2MCTLW_A
60A constant __UCA2STATW_A
60C constant __UCA2RXBUF_A
60E constant __UCA2TXBUF_A
610 constant __UCA2ABCCTL_A
612 constant __UCA2IRTCTL_A
613 constant __UCA2IRRCTL_A
61A constant __UCA2IFG_A
61C constant __UCA2IFG_A
61E constant __UCA2IV_A

620 constant __UCA3CTLW0_A
622 constant __UCA3CTLW1_A
626 constant __UCA3BRW_A
626 constant __UCA3BR0_A
627 constant __UCA3BR1_A
628 constant __UCA3MCTLW_A
62A constant __UCA3STATW_A
62C constant __UCA3RXBUF_A
62E constant __UCA3TXBUF_A
630 constant __UCA3ABCCTL_A
632 constant __UCA3IRTCTL_A
633 constant __UCA3IRRCTL_A
63A constant __UCA3IFG_A
63C constant __UCA3IFG_A
63E constant __UCA3IV_A

640 constant __UCB0CTLW0_A
642 constant __UCB0CTLW1_A
646 constant __UCB0BRW_A
646 constant __UCB0BR0_A
647 constant __UCB0BR1_A
648 constant __UCB0STATW_A
64A constant __UCB0TBCNT_A
64C constant __UCB0RXBUF_A
64E constant __UCB0TXBUF_A
654 constant __UCB0I2COA0_A
656 constant __UCB0I2COA1_A
658 constant __UCB0I2COA2_A
65A constant __UCB0I2COA3_A
65C constant __UCB0ADDRX_A
65E constant __UCB0ADDMASK_A
660 constant __UCB0I2CSA_A
66A constant __UCB0IE_A
66C constant __UCB0IFG_A
66E constant __UCA0IV_A

680 constant __UCB1CTLW0_A
682 constant __UCB1CTLW1_A
686 constant __UCB1BRW_A
686 constant __UCB1BR0_A
687 constant __UCB1BR1_A
688 constant __UCB1STATW_A
68A constant __UCB1TBCNT_A
68C constant __UCB1RXBUF_A
68E constant __UCB1TXBUF_A
694 constant __UCB1I2COA0_A
696 constant __UCB1I2COA1_A
698 constant __UCB1I2COA2_A
69A constant __UCB1I2COA3_A
69C constant __UCB1ADDRX_A
69E constant __UCB1ADDMASK_A
6A0 constant __UCB1I2CSA_A
6AA constant __UCB1IE_A
6AC constant __UCB1IFG_A
6AE constant __UCA1IV_A

6C0 constant __UCB2CTLW0_A
6C2 constant __UCB2CTLW1_A
6C6 constant __UCB2BRW_A
6C6 constant __UCB2BR0_A
6C7 constant __UCB2BR1_A
6C8 constant __UCB2STATW_A
6CA constant __UCB2TBCNT_A
6CC constant __UCB2RXBUF_A
6CE constant __UCB2TXBUF_A
6D4 constant __UCB2I2COA0_A
6D6 constant __UCB2I2COA1_A
6D8 constant __UCB2I2COA2_A
6DA constant __UCB2I2COA3_A
6DC constant __UCB2ADDRX_A
6DE constant __UCB2ADDMASK_A
6E0 constant __UCB2I2CSA_A
6EA constant __UCB2IE_A
6EC constant __UCB2IFG_A
6EE constant __UCA2IV_A

700 constant __UCB3CTLW0_A
702 constant __UCB3CTLW1_A
706 constant __UCB3BRW_A
706 constant __UCB3BR0_A
707 constant __UCB3BR1_A
708 constant __UCB3STATW_A
70A constant __UCB3TBCNT_A
70C constant __UCB3RXBUF_A
70E constant __UCB3TXBUF_A
714 constant __UCB3I2COA0_A
716 constant __UCB3I2COA1_A
718 constant __UCB3I2COA2_A
71A constant __UCB3I2COA3_A
71C constant __UCB3ADDRX_A
71E constant __UCB3ADDMASK_A
720 constant __UCB3I2CSA_A
72A constant __UCB3IE_A
72C constant __UCB3IFG_A
72E constant __UCA3IV_A

7C0 constant __TA4CTL_A
7C2 constant __TA4CTL0_A
7C4 constant __TA4CTL1_A
7C6 constant __TA4CTL2_A
7D0 constant __TA4R_A
7D2 constant __TA4CCR0_A
7D4 constant __TA4CCR1_A
7D6 constant __TA4CCR2_A
7E0 constant __TA4EX0_A
7EE constant __TA4IV_A

