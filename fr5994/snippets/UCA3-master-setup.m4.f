    01 __UCA3CTLW0_A **bis   \ UCA3CTLW0  Reset USCI
    bn 111 __P6SEL0_A *bis   \ P6SEL0  P6.0=MOSI, P6.1=MISO, P6.2=CLK
    bn 111 __P6SEL1_A *bic   \ P6SEL1  3-wire SPI
    bn 0100 __P6DIR_A *bis   \ P6.2 Resistor on
    bn 0100 __P6REN_A *bis   \ P6.2 pullup resistor
    400 __UCA3MCTLW_A !      \ 
    A981 __UCA3CTLW0_A !     \ UCA3CTLW0  Master using SMCLK
    08 __UCA3BRW_A !         \ UCA3BRW  Clock is 16Mhz/8 = 2MHz
    00 __P6IE_A c!           \ UCA3P5IE_A Not used must be zero!
    01 __UCA3CTLW0_A **bic   \ UCA3CTLW0  Free USCI
    bn 111 __P6OUT_A *bis    \ P6OUT  P6.0=MOSI, P5.6=MISO, P6.2=CLK
