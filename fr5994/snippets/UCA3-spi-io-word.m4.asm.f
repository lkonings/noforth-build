code SPI-I/O ( b1 -- b2 )    \ Read and write at SPI-bus
    begin,  #2 __UCA3IFG_A & bit  \ UCA3IFG
    cs? until,
    tos  __UCA3TXBUF_A & .b mov   \ UCA3TXBUF
    begin,  #1 __UCA3IFG_A & bit  \ UCA3IFG
    cs? until,
     __UCA3RXBUF_A &  tos .b mov  \ UCA3RXBUF
    next
end-code
