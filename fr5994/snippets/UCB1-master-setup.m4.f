    01 __UCB1CTLW0_A **bis   \ UCB1CTLW0  Reset USCI
    bn 111 __P5SEL0_A *bis   \ P5SEL0  P5.0=MOSI, P5.1=MISO, P5.2=CLK
    bn 111 __P5SEL1_A *bic   \ P5SEL1  3-wire SPI
    bn 0100 __P5DIR_A *bis   \ P5.2 Resistor on
    bn 0100 __P5REN_A *bis   \ P5.2 pullup resistor
    6981 __UCB1CTLW0_A !     \ UCB1CTLW0  Master using SMCLK
    08 __UCB1BRW_A !         \ UCB1BRW  Clock is 16Mhz/8 = 2MHz
    00 __P5IE_A c!           \ UCB1P5IE_A Not used must be zero!
    01 __UCB1CTLW0_A **bic   \ UCB1CTLW0  Free USCI
    bn 111 __P5OUT_A *bis    \ P5OUT  P5.0=MOSI, P5.1=MISO, P5.2=CLK
