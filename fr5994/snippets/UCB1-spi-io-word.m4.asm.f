code SPI-I/O ( b1 -- b2 )    \ Read and write at SPI-bus
    begin,  #2 __UCB1IFG_A & bit  \ UCB1IFG
    cs? until,
    tos  __UCB1TXBUF_A & .b mov   \ UCB1TXBUF
    begin,  #1 __UCB1IFG_A & bit  \ UCB1IFG
    cs? until,
     __UCB1RXBUF_A &  tos .b mov  \ UCB1RXBUF
    next
end-code
