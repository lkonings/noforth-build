hex

\ Store pattern b at leds
: >LEDS         ( b -- )  __P3OUT_A c! ( P3OUT ) ;

: FLASH         ( -- )          \ Visualise startup
    FF >leds  064 ms            \ All leds on
    00 >leds  064 ms ;          \ All leds off

: SETUP-PORTS   ( -- )
    00 __P3SEL0_A c!            \ P3SEL  Port all bits I/O
    00 __P3SEL1_A c!            \ P3SEL  Port all bits I/O
    FF __P3DIR_A c! ;           \ P3DIR  All bits of P3 are outputs

: COUNTER       ( -- )          \ Binary counter
    setup-ports  flash
    0                           \ Counter on stack
    begin
        1+                      \ Increase counter
        dup >leds
        020 ms                  \ Wait
    key? until
    drop ;

: RUNNER        ( -- )          \ A running light
    setup-ports  flash
    begin
        8 0 do                  \ Loop eight times
            1 i lshift  >leds   \ Make bitpattern
            064 ms              \ Wait
        loop
    key? until                  \ Until a key is pressed
    ;

freeze

