m4_define(__SFRIE1_A,100)m4_dnl
m4_define(__SFRIE1_A,102)m4_dnl
m4_define(__SFRRPCR_A,104)m4_dnl
m4_define(__PMMCTL0_A,120)m4_dnl
m4_define(__PMMFG_A,12A)m4_dnl
m4_define(__PM5CTL0_A,130)m4_dnl
m4_define(__FRCTL0_A,140)m4_dnl
m4_define(__GCCCTL0_A,144)m4_dnl
m4_define(__GCCCTL1_A,146)m4_dnl
m4_define(__CRC16DI_A,150)m4_dnl
m4_define(__CRCDIRB_A,152)m4_dnl
m4_define(__CRCINIRES_A,154)m4_dnl
m4_define(__CRCRESR_A,156)m4_dnl
m4_define(__RCCTL0_A,158)m4_dnl
m4_define(__WDTCTL_A,15C)m4_dnl
m4_define(__CSCTL0_A,160)m4_dnl
m4_define(__CSCTL1_A,162)m4_dnl
m4_define(__CSCTL2_A,164)m4_dnl
m4_define(__CSCTL3_A,166)m4_dnl
m4_define(__CSCTL4_A,168)m4_dnl
m4_define(__CSCTL5_A,16A)m4_dnl
m4_define(__CSCTL6_A,16C)m4_dnl
m4_define(__SYSCTL_A,180)m4_dnl
m4_define(__SYSJMBC_A,186)m4_dnl
m4_define(__SYSJMBI0_A,188)m4_dnl
m4_define(__SYSJMBI1_A,18A)m4_dnl
m4_define(__SYSJMBO0_A,18C)m4_dnl
m4_define(__SYSJMBO1_A,18E)m4_dnl
m4_define(__SYSUNIV_A,19A)m4_dnl
m4_define(__SYSRSIV_A,19C)m4_dnl
m4_define(__SYSRSTIV_A,19E)m4_dnl
m4_define(__REFCTL_A,1B0)m4_dnl
m4_define(__P1IN_A,200)m4_dnl
m4_define(__P1OUT_A,202)m4_dnl
m4_define(__P1DIR_A,204)m4_dnl
m4_define(__P1REN_A,206)m4_dnl
m4_define(__P1SEL0_A,20A)m4_dnl
m4_define(__P1SEL1_A,20C)m4_dnl
m4_define(__P1IV_A,20E)m4_dnl
m4_define(__P1SELC_A,210)m4_dnl
m4_define(__P1IES_A,218)m4_dnl
m4_define(__P1IE_A,21A)m4_dnl
m4_define(__P1IFG_A,21C)m4_dnl
m4_define(__P2IN_A,201)m4_dnl
m4_define(__P2OUT_A,203)m4_dnl
m4_define(__P2DIR_A,205)m4_dnl
m4_define(__P2REN_A,207)m4_dnl
m4_define(__P2SEL0_A,20B)m4_dnl
m4_define(__P2SEL1_A,20D)m4_dnl
m4_define(__P2IV_A,21E)m4_dnl
m4_define(__P2SELC_A,211)m4_dnl
m4_define(__P2IES_A,219)m4_dnl
m4_define(__P2IE_A,21B)m4_dnl
m4_define(__P2IFG_A,21D)m4_dnl
m4_define(__P3IN_A,220)m4_dnl
m4_define(__P3OUT_A,222)m4_dnl
m4_define(__P3DIR_A,224)m4_dnl
m4_define(__P3REN_A,226)m4_dnl
m4_define(__P3SEL0_A,22A)m4_dnl
m4_define(__P3SEL1_A,22C)m4_dnl
m4_define(__P3IV_A,22E)m4_dnl
m4_define(__P3SELC_A,230)m4_dnl
m4_define(__P3IES_A,238)m4_dnl
m4_define(__P3IE_A,23A)m4_dnl
m4_define(__P3IFG_A,23C)m4_dnl
m4_define(__P4IN_A,221)m4_dnl
m4_define(__P4OUT_A,223)m4_dnl
m4_define(__P4DIR_A,225)m4_dnl
m4_define(__P4REN_A,227)m4_dnl
m4_define(__P4SEL0_A,22B)m4_dnl
m4_define(__P4SEL1_A,22D)m4_dnl
m4_define(__P4IV_A,22E)m4_dnl
m4_define(__P4SELC_A,231)m4_dnl
m4_define(__P4IES_A,239)m4_dnl
m4_define(__P4IE_A,23B)m4_dnl
m4_define(__P4IFG_A,23D)m4_dnl
m4_define(__P5IN_A,240)m4_dnl
m4_define(__P5OUT_A,242)m4_dnl
m4_define(__P5DIR_A,244)m4_dnl
m4_define(__P5REN_A,246)m4_dnl
m4_define(__P5SEL0_A,24A)m4_dnl
m4_define(__P5SEL1_A,24C)m4_dnl
m4_define(__P5IV_A,24E)m4_dnl
m4_define(__P5SELC_A,250)m4_dnl
m4_define(__P5IES_A,258)m4_dnl
m4_define(__P5IE_A,25A)m4_dnl
m4_define(__P5IFG_A,25C)m4_dnl
m4_define(__P6IN_A,241)m4_dnl
m4_define(__P6OUT_A,243)m4_dnl
m4_define(__P6DIR_A,245)m4_dnl
m4_define(__P6REN_A,247)m4_dnl
m4_define(__P6SEL0_A,24B)m4_dnl
m4_define(__P6SEL1_A,24D)m4_dnl
m4_define(__P6IV_A,24E)m4_dnl
m4_define(__P6SELC_A,251)m4_dnl
m4_define(__P6IES_A,259)m4_dnl
m4_define(__P6IE_A,25B)m4_dnl
m4_define(__P6IFG_A,25D)m4_dnl
m4_define(__P7IN_A,260)m4_dnl
m4_define(__P7OUT_A,262)m4_dnl
m4_define(__P7DIR_A,264)m4_dnl
m4_define(__P7REN_A,266)m4_dnl
m4_define(__P7SEL0_A,26A)m4_dnl
m4_define(__P7SEL1_A,26C)m4_dnl
m4_define(__P7IV_A,26E)m4_dnl
m4_define(__P7SELC_A,260)m4_dnl
m4_define(__P7IES_A,268)m4_dnl
m4_define(__P7IE_A,26A)m4_dnl
m4_define(__P7IFG_A,26C)m4_dnl
m4_define(__P8IN_A,261)m4_dnl
m4_define(__P8OUT_A,263)m4_dnl
m4_define(__P8DIR_A,265)m4_dnl
m4_define(__P8REN_A,267)m4_dnl
m4_define(__P8SEL0_A,26B)m4_dnl
m4_define(__P8SEL1_A,26D)m4_dnl
m4_define(__P8IV_A,26E)m4_dnl
m4_define(__P8SELC_A,271)m4_dnl
m4_define(__P8IES_A,279)m4_dnl
m4_define(__P8IE_A,27B)m4_dnl
m4_define(__P8IFG_A,27D)m4_dnl
m4_define(__PJIN_A,320)m4_dnl
m4_define(__PJOUT_A,322)m4_dnl
m4_define(__PJDIR_A,324)m4_dnl
m4_define(__PJREN_A,326)m4_dnl
m4_define(__PJSEL0_A,32A)m4_dnl
m4_define(__PJSEL1_A,32C)m4_dnl
m4_define(__PJSELC_A,336)m4_dnl
m4_define(__TA0CTL_A,340)m4_dnl
m4_define(__TA0CTL0_A,342)m4_dnl
m4_define(__TA0CTL1_A,344)m4_dnl
m4_define(__TA0CTL2_A,346)m4_dnl
m4_define(__TA0R_A,350)m4_dnl
m4_define(__TA0CCR0_A,352)m4_dnl
m4_define(__TA0CCR1_A,354)m4_dnl
m4_define(__TA0CCR2_A,356)m4_dnl
m4_define(__TA0EX0_A,360)m4_dnl
m4_define(__TA0IV_A,36E)m4_dnl
m4_define(__TA1CTL_A,380)m4_dnl
m4_define(__TA1CTL0_A,382)m4_dnl
m4_define(__TA1CTL1_A,384)m4_dnl
m4_define(__TA1CTL2_A,386)m4_dnl
m4_define(__TA1R_A,390)m4_dnl
m4_define(__TA1CCR0_A,392)m4_dnl
m4_define(__TA1CCR1_A,394)m4_dnl
m4_define(__TA1CCR2_A,396)m4_dnl
m4_define(__TA1EX0_A,3A0)m4_dnl
m4_define(__TA1IV_A,3AE)m4_dnl
m4_define(__TB0CTL_A,3C0)m4_dnl
m4_define(__TB0CTL0_A,3C2)m4_dnl
m4_define(__TB0CTL1_A,3C4)m4_dnl
m4_define(__TB0CTL2_A,3C6)m4_dnl
m4_define(__TB0CTL3_A,3C8)m4_dnl
m4_define(__TB0CTL4_A,3CA)m4_dnl
m4_define(__TB0CTL5_A,3CC)m4_dnl
m4_define(__TB0CTL6_A,3CE)m4_dnl
m4_define(__TB0R_A,3D0)m4_dnl
m4_define(__TB0CCR0_A,3D2)m4_dnl
m4_define(__TB0CCR1_A,3D4)m4_dnl
m4_define(__TB0CCR2_A,3D6)m4_dnl
m4_define(__TB0CCR3_A,3D8)m4_dnl
m4_define(__TB0CCR4_A,3DA)m4_dnl
m4_define(__TB0CCR5_A,3DC)m4_dnl
m4_define(__TB0CCR6_A,3DE)m4_dnl
m4_define(__TB0EX0_A,3E0)m4_dnl
m4_define(__TB00IV_A,3EE)m4_dnl
m4_define(__UCB1CTLW0,680)m4_dnl
m4_define(__UCB1CTLW1,682)m4_dnl
m4_define(__UCB1BRW,686)m4_dnl
m4_define(__UCB1BR0,686)m4_dnl
m4_define(__UCB1BR1,687)m4_dnl
m4_define(__UCB1STAT,688)m4_dnl
m4_define(__UCB1RXBUF,68C)m4_dnl
m4_define(__UCB1TXBUF,68E)m4_dnl
m4_define(__UCB1I2CSA,6A0)m4_dnl
m4_define(__UCB1IFG,6AC)m4_dnl
m4_define(__RED,01)m4_dnl
m4_define(__RED_POUT,01 202)m4_dnl
m4_define(__RED_ON,01 202 bis*)m4_dnl
m4_define(__RED_OFF,01 202 bic*)m4_dnl
m4_define(__GREEN,02)m4_dnl
m4_define(__GREEN_POUT,40 202)m4_dnl
m4_define(__GREEN_ON,40 202 bis*)m4_dnl
m4_define(__GREEN_OFF,40 202 bic*)m4_dnl
