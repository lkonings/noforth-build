

(* This version eUSCI_B0 runs on noForth C5994 version 200202. Do not the use
   the low power version, because this code reuses MS# of the new MS-routine ;)

  LK

  USCI hardware SPI on MSP430G2553 using port-1 & port-2.
  SPI i/o interfacing the nRF24L01 with two or more Launchpad boards
  Micro Launchpads and/or Egel kits.

  Connect the SPI lines of USCIB P1.5=CLOCKPULSE, P1.6=DATA-IN, P1.7=DATA-OUT
  P1.4=CSN,  P2.3=CE of the nRF24L01. On the Egel kit it's just putting the
  module in the connector marked nRF24L01!!

  Note that decoupling is very important right near the nRF24l01 module. The
  Egel kit vsn-2 has an extra 22uF near the power connections. The Launchpad
  and the Egel kit vsn-1 need an extra 10uF or more for decoupling!!

  More info on page 445 of SLAU144J.PDF  Configuration of the pins on
  page 49 of SLAS735J.PDF

  The user words are: CHECK CARRIER PULSE & WAVE

MSP430FR5994

 Addresses  - Labels       - Bit patterns
 1CC   15C  - WDTCL        - Off already
 20A   20A  - P1SEL0       - 00C
 20C   20C  - P1SEL1       - 00C
 680   680  - UCB1CTLW0    - FF1
 682   682  - UCB1CTLW1    - 000
 686   686  - UCB1BR0      - 050
 688   688  - UCB1STATW    - sss USCI status
 68C   68C  - UCB1RXBUF    - rrr RX Data
 68E   68E  - UCB1TXBUF    - ttt TX Data
 694   694  - UCB1I2C0A    - ooo NC
 6A0   6A0  - UCB1I2CSA    - 042 /2
 6AA   6AA  - UCB1CIE      - 000 USCI interrupt enable
 6AC   6AC  - UCB1IFG      - 002 = TX ready, 001 = RX ready

 NO MCTL

Label    P1  P2  P3  P4  P5  P6  P7  P8  PJ   Function
-----------------------------------------------------
PxIN     200 201 220 221 240 241 260 261 320  Input
PxOUT    202 203 222 223 242 243 262 263 322  Output
PxDIR    204 205 224 225 244 245 264 265 324  Direction
PxREN    206 207 226 227 246 247 266 267 326  Resistor enable
PxSEL0   20A 20B 22A 229 24A 24B 26A 26B 32A  Select 0
PxSEL1   20C 20D 22C 22D 24C 24D 26C 26D 32C  Select 1
PxIV     20E 21E 22E 22F 24E 24E 26E 26E      Interrupt vector word
PxSELC   210 211 230 231 250 251 260 271 336  Complement selection
PxIES    218 219 238 239 258 259 268 279      Interrupt edge select
PxIE     21A 21B 23A 23B 25A 25B 26A 27B      Interrupt on
PxIFG    21C 21D 23C 23D 25C 25D 26C 27D      Interrupt flag

  SPI USCI master

                     MSP430FR5994
               ^  -----------------
              /|\|              XIN|- Optional 32.768 kHz xtal
               | |                 |
               --|RST          XOUT|- Idem
                 |                 |
           IRQ ->|P3.1         P5.0|-> Data Out (UCB1SIMO)
                 |                 |
            CE <-|P3.2         P5.1|<- Data In (UCB1SOMI)
                 |                 |
           CSN <-|P3.3         P5.2|-> Serial Clock Out (UCB1CLK)
                 |                 |
                 |             P?.?|-> Power out

  Concept: Willem Ouwerkerk & Jan van Kleef, october 2014
  Current version: Willem Ouwerkerk, januari 2019

  Launchpad documentation for USCI SPI

P3 & P5 are used for interfacing the nRF24L01+

P3.1  - IRQ                     \ Active low output   x0=Interrupt
P3.2  - CE                      \ Device enable high  x1=Enable
P3.3  - CSN                     \ SPI enable low      x1=Select

P5.0  - DATA-OUT                \ Data bitstream out  x1=Mosi
P5.1  - DATA-IN                 \ Data bitstream in   x0=Miso
P5.2  - CLOCKPULSE              \ Clock               x1=Clock

P5.5  - Switch S2
P5.6  - Switch S1

P2.0  - Led red                 \ XEMIT Red led
P2.1  - Led green               \ Green led

P?.?  - Power led               \ Power output 2 Amp.
P?.?  - Analog input            \ 0 to 3V3

nRF basic timing:
    Max. SPI clock:         8 MHz
    Powerdown to standby:   1,5 ms
    Standby to TX/RX:       0,13 ms
    Transmit pulse CE high: 0,01 ms

Sensitivity: 2Mbps � -83dB, 1Mbps � 87dB, 250kbps � -96dB.
Receiving current: 2Mbps � 15mA, 1Mbps � 14.5mA, 250kbps � 14mA

On board PCB antenna, transmission distance reach 240M in open area, but
2.4G frequency is not good to pass through walls, also interfere by
2.4G wifi signal significantly.

Record format #pay ( 17 ) bytes:
Node 0:  |Admin|Dest.|Orig.|Sub node|Command|D0| to |D7|  Command
Node X:  |Admin|Dest.|Orig.|Sub node|Command|D0| to |D7|  (Re)Transmit
Node Y:  |Admin|Dest.|Orig.|Sub node|Command|D0| to |D7|  Answer

00 pay>       = Administration byte
01 pay>       = Destination node
02 pay>       = Origin node
03 pay>       = Address of sub node at destination
04 pay>       = Command for destination node
05 to 11 pay> = Data bytes (0C for now)

*)

extra\
1 constant #me

hex
value T?                \ Tracer on/off
: TEMIT     t? if  dup emit  then  drop ;  \ Show copy of char
: TRON      true to t? ;    : TROFF     false to t? ;
: RED-ON    01 203 *bis ;    : RED-OFF   01 203 *bic ;
: /MS       ( u -- )    ms# >r  r@ 0A / to ms#  ms  r> to ms# ;


code IRQ?   ( -- flag ) \ Flag is true when IRQ = low
    tos sp -) mov
    02 # 220 & .b bit \ P3IN
    tos tos subc
    next                \ next
end-code


                    ( USCI-B SPI interface to nRF24L01 )
: SPI-SETUP     ( -- )
    01 680 **bis      \ UCB1CTLW0  Reset USCI
    bn 111 24A *bis   \ P5SEL0  P5.0=MOSI, P5.1=MISO, P5.2=CLK
    bn 111 24C *bic   \ P5SEL1  3-wire SPI
    bn 0100 244 *bis    \ P5.2 Resistor on
    bn 0100 246 *bis    \ P5.2 pullup resistor
    6981 680 !       \ UCB1CTLW0  Master using SMCLK
    08 686 !           \ UCB1BRW  Clock is 16Mhz/8 = 2MHz
    00 25A c!           \ UCB1P5IE_A Not used must be zero!
    01 680 **bic      \ UCB1CTLW0  Free USCI
    bn 111 242 *bis    \ P5OUT  P5.0=MOSI, P5.1=MISO, P5.2=CLK
    bn 1000 222 c!    \ P3OUT  P3.1 irq=0.  P3.2 ce=0, P3.3 csn=1
    01 204 c!      \ P1DIR  P1.0=Red led
    01 202 c!      \ P1OUT  Red led off
    ;

code SPI-I/O ( b1 -- b2 )    \ Read and write at SPI-bus
    begin,  #2 6AC & bit    \ UCB1IFG
    cs? until,
    tos  68E & .b mov       \ UCB1TXBUF
    begin,  #1 6AC & bit    \ UCB1IFG
    cs? until,
     68C &  tos .b mov      \ UCB1RXBUF
    next
end-code

: SPI-OUT       ( b -- )    spi-i/o drop ;  \ Write x to SPI-bus
: SPI-IN        ( -- b )    0 spi-i/o ;     \ Read b from SPI-bus
: DEACTIVATE    ( -- )      
    bn 1000 222 *bis ;    \ P3OUT  SPI off CSN high


                ( Read and write to and from nRF24L01 )

55 constant #CH     \ Used channel number
280 constant #IRQ   \ Delay loops for XEMIT)
: RMASK    ( b1 -- b2 )    1F and ;
: WMASK    ( b1 -- b2 )    1F and  20 or ;

\ The first written byte returns internal status always
\ It is saved in the value STATUS using SPI-COMMAND
: GET-STATUS    ( -- s )    bn 1000 222 *bic  FF spi-i/o  deactivate ; \ CSN
: SPI-COMMAND   ( c -- )    bn 1000 222 *bic  spi-i/o drop ; \ CSN

\ Reading and writing to registers in the 24L01
: READ-REG      ( r -- b )  rmask spi-command  spi-in  deactivate ;
: WRITE-REG     ( b r -- )  wmask spi-command  spi-out deactivate ;

\ Read and write the communication addresses, of pipe-0 default: E7E7E7E7E7
: WRITE-ADDR  ( trxa -- )  wmask spi-command  5 for spi-out next  deactivate ;


                ( nRF24L01 control commands and setup )

\ Empty RX or TX data pipe
: FLUSH-RX      ( -- )      E2 spi-command deactivate ; \ Remove received data
: FLUSH-TX      ( -- )      E1 spi-command deactivate ; \ Remove transmitted data
: RESET         ( -- )      70 07 write-reg ;           \ Reset IRQ flags
: PIPES-ON      ( mask -- ) 3F and  02 write-reg ;      \ Set active pipes
: WAKEUP        ( -- )      0E 00 write-reg ; \ CRC 2 bytes, Powerup
: >CHANNEL      ( +n -- )   05 write-reg ;    \ Change RF-channel 7-bits
: >NRF          ( +n -- )   06 write-reg ;    \ 26 = 250kbps, full power
                                              \ 06 = 1 Mbit, full power (0 dBm)
                                              \ 00 = 1 Mbit, -18 dBm = lowest power
                                              \ 0E - 2 Mbit, full power
                                              \ 0C = 2 Mbit, -6 dBm = lower power


\ Elementary command set for the nRF24L01
value NRF               \ Contains choosen nRF24 RF setup
: DEFAULT-RF    ( -- )  06 to nrf ; \ Set default RF mode
default-rf

11 constant #PAY        \ Payload size 17 bytes
: SETUP24L01    ( -- )
    0C 0 write-reg      \ Enable CRC, 2 bytes
    03 01 write-reg     \ Auto Ack pipe 0 & 1
    02 pipes-on         \ Pipe 1 on
    F0 F0 F0 F0 #me 0B write-addr \ Set ME own address
    03 03 write-reg     \ Five byte address
    1F 04 write-reg     \ Retry after 500 us & 15 retry's
    #ch >channel        \ channel #CH to start with
    nrf >nrf            \ 1 Mbps, max. power
    #pay 11 write-reg   \ #pay bytes payload in P0
    #pay 12 write-reg   \ #pay bytes payload in P1
    reset               \ Enable CRC, 2 bytes & reset flags
    flush-rx  flush-tx  \ Start empty
    wakeup  0F /ms      \ Power up
    red-off ;           \ Led off


\ Format: Admin, Dest. node, Org. node, Sub node, Command, Data-0, Data-1, Data-2, Data-3
create 'READ    #pay allot  \ Receive buffer
create 'WRITE   #pay allot  \ Transmit buffer
: READ-RX   ( -- )      \ Receive #PAY bytes
    61 spi-command  'read #pay bounds
    ?do  spi-in i c!  loop   deactivate ;

: WRITE-TXA ( -- )      \ Send #PAY bytes with Ack check
    A0 spi-command  'write #pay bounds
    ?do  i c@ spi-out  loop  deactivate
    bn 100 222 *bis  5 for next  bn 100 222 *bic ; \ P3OUT  Transmit pulse on CE


: '>PAY     ( +n -- a )     'write + ;  \ Leave address of TX payload
: >PAY      ( b +n -- )     '>pay c! ;  \ Store byte for TX payload
: 'PAY>     ( +n -- a )     'read + ;   \ Leave address of RX payload
: PAY>      ( +n -- b )     'pay> c@ ;  \ Read byte from RX payload

: .STATUS   ( -- )
    ." Node " #me . ." nRF24"   \ Which node with nRf24
    get-status ?dup 0= ?exit    \ Not connected?
    0E <> if  ."  not"          \ Busy?
    then  ."  ok " ;            \ No, startup condition


                    ( Send and receive commands for nRF24L01 )

: WRITE-MODE    ( -- )          \ Power up module as transmitter
    bn 100 222 *bic  wakeup          \ P3OUT  CE low, receive off, wakeup transmitter
    reset  1 pipes-on  2 /ms ;  \ Reset flags & pipe-0 active, wait 200 microsec.

: READ-MODE     ( -- )
    0F 00 write-reg  2 pipes-on \ Power up module as receiver, activate pipe-1
    reset  bn 100 222 *bis  2 /ms ;  \ P3OUT  Enable receive mode, wait 200 microsec.


: XEMIT?        ( c -- +n )     \ +n = 0A when transmit has failed
    4 >pay  0  0A 0 do          \ Try it 0A times * ARC = dm 150 times max.
        write-txa  #irq 0 ?do   \ Send payload
            irq? if leave then  \ Wait for IRQ  
        loop  
        07 read-reg 20 and if  leave  then \ Ready when it was an Ack! (TX?)
        flush-tx  reset  1+     \ Reset flags & empty pipeline, count failures
        dup /ms  #ch >channel   \ Variable repeat time, clear packet loss
    loop
    flush-tx  reset ;

value #FAIL     \ Note XEMIT failures
: BUSY      ( -- )
    read-mode  40 for                   \ Wait shortly while a channel is busy
        4 /ms  09 read-reg 0=           \ Check for no carrier?
        if rdrop exit then  ch . temit  \ Ready when no carrier
    next ;

\ Node primitive XEMIT now with retry & restart after a failed XEMIT?
: XEMIT     ( c -- )
    0 to #fail  begin
        red-on
        busy  write-mode  dup temit \ Check carrier, show echo
        dup xemit? 0A <> if         \ Send payload
            drop  red-off  read-mode  exit               
        else
            red-off  incr #fail  setup24l01 \ TX failed, reinit.
       then
        #fail #me 1+ 2* * /ms       \ Variable retry time
    #fail 20 = until
    drop  read-mode ;

: XKEY          ( -- c )
    begin
        begin irq? until        \ Something happened?
    07 read-reg 40 and 0= while \ Is it a payload (RX?)
        setup24l01  read-mode   \ No, restart 24L01
    repeat
    read-rx  4 pay>     \ Yes, read command from packet
    reset  flush-rx     \ Empty pipeline
    bn 100 222 *bic ;         \ To standby II - ce=0

\ Set destination address to node from stack, receive address is my "me"
: SET-DEST      ( node -- )
    dup >r  1 >pay  #me 2 >pay      \ Set Destination & origin nodes
    F0 F0 F0 F0 r@ 0A write-addr    \ Receive address P0
    F0 F0 F0 F0 r> 10 write-addr ;  \ Transmit address

: setup
    spi-setup
    setup24L01
    cr ." .status: " .status
    cr ." get-status: " get-status .  ;


shield 24L01\   freeze

\ End ;;;

: XEMITS dm 26 0 do 41 i + xemit loop ;

setup
