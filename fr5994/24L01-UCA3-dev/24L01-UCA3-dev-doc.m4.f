(* This version eUSCI_B0 runs on noForth C5994 version 200202. Do not the use
   the low power version, because this code reuses MS# of the new MS-routine ;)

  LK

  USCI hardware SPI on MSP430G2553 using port-1 & port-2.
  SPI i/o interfacing the nRF24L01 with two or more Launchpad boards
  Micro Launchpads and/or Egel kits.

  Connect the SPI lines of USCIB P1.5=CLOCKPULSE, P1.6=DATA-IN, P1.7=DATA-OUT
  P1.4=CSN,  P2.3=CE of the nRF24L01. On the Egel kit it's just putting the
  module in the connector marked nRF24L01!!

  Note that decoupling is very important right near the nRF24l01 module. The
  Egel kit vsn-2 has an extra 22uF near the power connections. The Launchpad
  and the Egel kit vsn-1 need an extra 10uF or more for decoupling!!

  More info on page 445 of SLAU144J.PDF  Configuration of the pins on
  page 49 of SLAS735J.PDF

  The user words are: CHECK CARRIER PULSE & WAVE

MSP430FR5994

 Addresses  - Labels       - Bit patterns
 1CC   15C  - WDTCL        - Off already
 20A   20A  - P1SEL0       - 00C
 20C   20C  - P1SEL1       - 00C
 680   680  - UCB1CTLW0    - FF1
 682   682  - UCB1CTLW1    - 000
 686   686  - UCB1BR0      - 050
 688   688  - UCB1STATW    - sss USCI status
 68C   68C  - UCB1RXBUF    - rrr RX Data
 68E   68E  - UCB1TXBUF    - ttt TX Data
 694   694  - UCB1I2C0A    - ooo NC
 6A0   6A0  - UCB1I2CSA    - 042 /2
 6AA   6AA  - UCB1CIE      - 000 USCI interrupt enable
 6AC   6AC  - UCB1IFG      - 002 = TX ready, 001 = RX ready

 NO MCTL

Label    P1  P2  P3  P4  P5  P6  P7  P8  PJ   Function
-----------------------------------------------------
PxIN     200 201 220 221 240 241 260 261 320  Input
PxOUT    202 203 222 223 242 243 262 263 322  Output
PxDIR    204 205 224 225 244 245 264 265 324  Direction
PxREN    206 207 226 227 246 247 266 267 326  Resistor enable
PxSEL0   20A 20B 22A 229 24A 24B 26A 26B 32A  Select 0
PxSEL1   20C 20D 22C 22D 24C 24D 26C 26D 32C  Select 1
PxIV     20E 21E 22E 22F 24E 24E 26E 26E      Interrupt vector word
PxSELC   210 211 230 231 250 251 260 271 336  Complement selection
PxIES    218 219 238 239 258 259 268 279      Interrupt edge select
PxIE     21A 21B 23A 23B 25A 25B 26A 27B      Interrupt on
PxIFG    21C 21D 23C 23D 25C 25D 26C 27D      Interrupt flag

  SPI USCI master

                     MSP430FR5994
               ^  -----------------
              /|\|              XIN|- Optional 32.768 kHz xtal
               | |                 |
               --|RST          XOUT|- Idem
                 |                 |
           IRQ ->|P3.1         P5.0|-> Data Out (UCB1SIMO)
                 |                 |
            CE <-|P3.2         P5.1|<- Data In (UCB1SOMI)
                 |                 |
           CSN <-|P3.3         P5.2|-> Serial Clock Out (UCB1CLK)
                 |                 |
                 |             P?.?|-> Power out

  Concept: Willem Ouwerkerk & Jan van Kleef, october 2014
  Current version: Willem Ouwerkerk, januari 2019

  Launchpad documentation for USCI SPI

P3 & P5 are used for interfacing the nRF24L01+

P3.1  - IRQ                     \ Active low output   x0=Interrupt
P3.2  - CE                      \ Device enable high  x1=Enable
P3.3  - CSN                     \ SPI enable low      x1=Select

P5.0  - DATA-OUT                \ Data bitstream out  x1=Mosi
P5.1  - DATA-IN                 \ Data bitstream in   x0=Miso
P5.2  - CLOCKPULSE              \ Clock               x1=Clock

P5.5  - Switch S2
P5.6  - Switch S1

P2.0  - Led red                 \ XEMIT Red led
P2.1  - Led green               \ Green led

P?.?  - Power led               \ Power output 2 Amp.
P?.?  - Analog input            \ 0 to 3V3

nRF basic timing:
    Max. SPI clock:         8 MHz
    Powerdown to standby:   1,5 ms
    Standby to TX/RX:       0,13 ms
    Transmit pulse CE high: 0,01 ms

Sensitivity: 2Mbps � -83dB, 1Mbps � 87dB, 250kbps � -96dB.
Receiving current: 2Mbps � 15mA, 1Mbps � 14.5mA, 250kbps � 14mA

On board PCB antenna, transmission distance reach 240M in open area, but
2.4G frequency is not good to pass through walls, also interfere by
2.4G wifi signal significantly.

Record format #pay ( 17 ) bytes:
Node 0:  |Admin|Dest.|Orig.|Sub node|Command|D0| to |D7|  Command
Node X:  |Admin|Dest.|Orig.|Sub node|Command|D0| to |D7|  (Re)Transmit
Node Y:  |Admin|Dest.|Orig.|Sub node|Command|D0| to |D7|  Answer

00 pay>       = Administration byte
01 pay>       = Destination node
02 pay>       = Origin node
03 pay>       = Address of sub node at destination
04 pay>       = Command for destination node
05 to 11 pay> = Data bytes (0C for now)

*)

