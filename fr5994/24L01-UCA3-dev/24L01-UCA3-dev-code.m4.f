extra\
1 constant #me

hex
value T?                \ Tracer on/off
: TEMIT     t? if  dup emit  then  drop ;  \ Show copy of char
: TRON      true to t? ;    : TROFF     false to t? ;
: RED-ON    __RED __P2OUT_A *bis ;    : RED-OFF   __RED __P2OUT_A *bic ;
: /MS       ( u -- )    ms# >r  r@ 0A / to ms#  ms  r> to ms# ;


code IRQ?   ( -- flag ) \ Flag is true when IRQ = low
    tos sp -) mov
    02 # __P3IN_A & .b bit  \ P3IN
    tos tos subc
    next                \ next
end-code


                    ( USCI-B SPI interface to nRF24L01 )
: SPI-SETUP     ( -- )
m4_include(fr5994/snippets/UCA3-master-setup.m4.f)m4_dnl
    bn 1000 __P3OUT_A c!  \ P3OUT  P3.1 irq=0.  P3.2 ce=0, P3.3 csn=1
    __RED __P1DIR_A c!   \ P1DIR  P1.0=Red led
    __RED __P1OUT_A c!   \ P1OUT  Red led off
    ;

m4_include(fr5994/snippets/UCA3-spi-io-word.m4.asm.f)m4_dnl

: SPI-OUT       ( b -- )    spi-i/o drop ;  \ Write x to SPI-bus
: SPI-IN        ( -- b )    0 spi-i/o ;     \ Read b from SPI-bus
: DEACTIVATE    ( -- )      
    bn 1000 __P3OUT_A *bis ;    \ P3OUT  SPI off CSN high


                ( Read and write to and from nRF24L01 )

55 constant #CH     \ Used channel number
280 constant #IRQ   \ Delay loops for XEMIT)
: RMASK    ( b1 -- b2 )    1F and ;
: WMASK    ( b1 -- b2 )    1F and  20 or ;

\ The first written byte returns internal status always
\ It is saved in the value STATUS using SPI-COMMAND
: GET-STATUS    ( -- s )    bn 1000 __P3OUT_A *bic  FF spi-i/o  deactivate ; \ CSN
: SPI-COMMAND   ( c -- )    bn 1000 __P3OUT_A *bic  spi-i/o drop ; \ CSN

\ Reading and writing to registers in the 24L01
: READ-REG      ( r -- b )  rmask spi-command  spi-in  deactivate ;
: WRITE-REG     ( b r -- )  wmask spi-command  spi-out deactivate ;

\ Read and write the communication addresses, of pipe-0 default: E7E7E7E7E7
: WRITE-ADDR  ( trxa -- )  wmask spi-command  5 for spi-out next  deactivate ;


                ( nRF24L01 control commands and setup )

\ Empty RX or TX data pipe
: FLUSH-RX      ( -- )      E2 spi-command deactivate ; \ Remove received data
: FLUSH-TX      ( -- )      E1 spi-command deactivate ; \ Remove transmitted data
: RESET         ( -- )      70 07 write-reg ;           \ Reset IRQ flags
: PIPES-ON      ( mask -- ) 3F and  02 write-reg ;      \ Set active pipes
: WAKEUP        ( -- )      0E 00 write-reg ; \ CRC 2 bytes, Powerup
: >CHANNEL      ( +n -- )   05 write-reg ;    \ Change RF-channel 7-bits
: >NRF          ( +n -- )   06 write-reg ;    \ 26 = 250kbps, full power
                                              \ 06 = 1 Mbit, full power (0 dBm)
                                              \ 00 = 1 Mbit, -18 dBm = lowest power
                                              \ 0E - 2 Mbit, full power
                                              \ 0C = 2 Mbit, -6 dBm = lower power


\ Elementary command set for the nRF24L01
value NRF               \ Contains choosen nRF24 RF setup
: DEFAULT-RF    ( -- )  06 to nrf ; \ Set default RF mode
default-rf

11 constant #PAY        \ Payload size 17 bytes
: SETUP24L01    ( -- )
    0C 0 write-reg      \ Enable CRC, 2 bytes
    03 01 write-reg     \ Auto Ack pipe 0 & 1
    02 pipes-on         \ Pipe 1 on
    F0 F0 F0 F0 #me 0B write-addr \ Set ME own address
    03 03 write-reg     \ Five byte address
    1F 04 write-reg     \ Retry after 500 us & 15 retry's
    #ch >channel        \ channel #CH to start with
    nrf >nrf            \ 1 Mbps, max. power
    #pay 11 write-reg   \ #pay bytes payload in P0
    #pay 12 write-reg   \ #pay bytes payload in P1
    reset               \ Enable CRC, 2 bytes & reset flags
    flush-rx  flush-tx  \ Start empty
    wakeup  0F /ms      \ Power up
    red-off ;           \ Led off


\ Format: Admin, Dest. node, Org. node, Sub node, Command, Data-0, Data-1, Data-2, Data-3
create 'READ    #pay allot  \ Receive buffer
create 'WRITE   #pay allot  \ Transmit buffer
: READ-RX   ( -- )      \ Receive #PAY bytes
    61 spi-command  'read #pay bounds
    ?do  spi-in i c!  loop   deactivate ;

: WRITE-TXA ( -- )      \ Send #PAY bytes with Ack check
    A0 spi-command  'write #pay bounds
    ?do  i c@ spi-out  loop  deactivate
    bn 100 __P3OUT_A *bis  5 for next  bn 100 __P3OUT_A *bic ; \ P3OUT  Transmit pulse on CE


: '>PAY     ( +n -- a )     'write + ;  \ Leave address of TX payload
: >PAY      ( b +n -- )     '>pay c! ;  \ Store byte for TX payload
: 'PAY>     ( +n -- a )     'read + ;   \ Leave address of RX payload
: PAY>      ( +n -- b )     'pay> c@ ;  \ Read byte from RX payload

: .STATUS   ( -- )
    ." Node " #me . ." nRF24"   \ Which node with nRf24
    get-status ?dup 0= ?exit    \ Not connected?
    0E <> if  ."  not"          \ Busy?
    then  ."  ok " ;            \ No, startup condition


                    ( Send and receive commands for nRF24L01 )

: WRITE-MODE    ( -- )          \ Power up module as transmitter
    bn 100 __P3OUT_A *bic  wakeup          \ P3OUT  CE low, receive off, wakeup transmitter
    reset  1 pipes-on  2 /ms ;  \ Reset flags & pipe-0 active, wait 200 microsec.

: READ-MODE     ( -- )
    0F 00 write-reg  2 pipes-on \ Power up module as receiver, activate pipe-1
    reset  bn 100 __P3OUT_A *bis  2 /ms ;  \ P3OUT  Enable receive mode, wait 200 microsec.


: XEMIT?        ( c -- +n )     \ +n = 0A when transmit has failed
    4 >pay  0  0A 0 do          \ Try it 0A times * ARC = dm 150 times max.
        write-txa  #irq 0 ?do   \ Send payload
            irq? if leave then  \ Wait for IRQ  
        loop  
        07 read-reg 20 and if  leave  then \ Ready when it was an Ack! (TX?)
        flush-tx  reset  1+     \ Reset flags & empty pipeline, count failures
        dup /ms  #ch >channel   \ Variable repeat time, clear packet loss
    loop
    flush-tx  reset ;

value #FAIL     \ Note XEMIT failures
: BUSY      ( -- )
    read-mode  40 for                   \ Wait shortly while a channel is busy
        4 /ms  09 read-reg 0=           \ Check for no carrier?
        if rdrop exit then  ch . temit  \ Ready when no carrier
    next ;

\ Node primitive XEMIT now with retry & restart after a failed XEMIT?
: XEMIT     ( c -- )
    0 to #fail  begin
        red-on
        busy  write-mode  dup temit \ Check carrier, show echo
        dup xemit? 0A <> if         \ Send payload
            drop  red-off  read-mode  exit               
        else
            red-off  incr #fail  setup24l01 \ TX failed, reinit.
       then
        #fail #me 1+ 2* * /ms       \ Variable retry time
    #fail 20 = until
    drop  read-mode ;

: XKEY          ( -- c )
    begin
        begin irq? until        \ Something happened?
    07 read-reg 40 and 0= while \ Is it a payload (RX?)
        setup24l01  read-mode   \ No, restart 24L01
    repeat
    read-rx  4 pay>     \ Yes, read command from packet
    reset  flush-rx     \ Empty pipeline
    bn 100 __P3OUT_A *bic ;         \ To standby II - ce=0

\ Set destination address to node from stack, receive address is my "me"
: SET-DEST      ( node -- )
    dup >r  1 >pay  #me 2 >pay      \ Set Destination & origin nodes
    F0 F0 F0 F0 r@ 0A write-addr    \ Receive address P0
    F0 F0 F0 F0 r> 10 write-addr ;  \ Transmit address

: setup
    spi-setup
    setup24L01
    cr ." .status: " .status
    cr ." get-status: " get-status .  ;


shield 24L01\   freeze

\ End ;;;

: XEMITS dm 26 0 do 41 i + xemit loop ;

setup
