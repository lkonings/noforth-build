extra\

hex
: MASTER-SETUP  ( -- )
m4_include(fr5994/snippets/UCB1-master-setup.m4.f)m4_dnl
__RED __P1DIR_A c!      \ P1DIR  P1.0=Red led
__RED __P1OUT_A c!      \ P1OUT  Red led off
    ;

: SLAVE-SETUP   ( -- )
    01 __UCB1CTLW0_A **bis   \ UCB1CTLW0  Reset USCI
    bn 111 __P5SEL0_A *bis   \ P5SEL0  P5.0=MOSI, P5.1=MISO, P5.2=CLK
    bn 111 __P5SEL1_A *bic   \ P5SEL1  3-wire SPI
    bn 0100 __P5DIR_A *bis   \ P5.2 Resistor on
    bn 0100 __P5REN_A *bis   \ P5.2 pullup resistor
    6181 __UCB1CTLW0_A !     \ UCB1CTLW0  Slave
    01 __UCB1CTLW0_A **bic   \ UCB1CTLW0  Free USCI
    bn 100 __P5DIR_A *bic    \ P5DIR  P5.2 = ingang
    begin  bn 100 __P5IN_A bit* until   \ P5IN  Master  in SPI mode (clock high)?
    ;

m4_include(fr5994/snippets/UCB1-spi-io-word.m4.asm.f)m4_dnl

: >SPI      ( u -- )    spi-i/o drop ;
: SPI>      ( -- u )    0 spi-i/o ;


\ The SPI master send  the loop-index and receives & displays the answer
: SPI-MASTER    ( -- )
    master-setup
    begin
        100 0 do
            cr i u.  i spi-i/o u. 50 ms
            stop? if leave  then
        loop
    stop? until ;

: FLASH         ( -- )  __RED __P1OUT_A *bis  64 ms  __RED __P1OUT_A *bic  64 ms ;   \ P1OUT

\ The SPI slave receives data from the master puts it on the leds
\ and sends the same data back, this first example can be made
\ to do something usefull!!
: SPI-SLAVE1     ( -- )
    \ -1 02A c!  0 02E c!  flash  \ P2DIR, P2SEL
    slave-setup  0 __UCB1TXBUF_A c!  0    \ UCB0TXBUF 
    begin
        \ spi-i/o dup 029 c!      \ P2OUT
        \ bn 10 __P5OUT_A *bis   \ P5OUT  P5.1=MISO
        spi-i/o dup .
    stop? until drop ;

: DEMO-SPI  ( u1 -- u2 )
    begin  01 __UCB1IFG_A bit* until  __UCB1RXBUF_A c@        \ IFG2  RX?
    begin  02 __UCB1IFG_A bit* until  dup __UCB1TXBUF_A c! ;  \ IFG2  TX?

\ This is the SPI slave demo from TI, it does nothing usefull however
: SPI-SLAVE2     ( -- )
    \ -1 02A c!  0 02E c!  flash  \ P2DIR, P2SEL
    slave-setup  0 __UCB1TXBUF_A c!  0    \ UCB0TXBUF 
    begin
        \ demo-spi  dup 029 c!    \ P2OUT
        spi-i/o dup .
    stop? until drop ;

: DEACTIVATE    ( -- )      
    bn 1000 __P3OUT_A *bis ;    \ P3OUT  SPI off CSN high

\ The first written byte returns internal status always
\ It is saved in the value STATUS using SPI-COMMAND
: GET-STATUS    ( -- s )    bn 1000 __P3OUT_A *bic  FF spi-i/o deactivate ; \ CSN
 
 shield SPI\  freeze

