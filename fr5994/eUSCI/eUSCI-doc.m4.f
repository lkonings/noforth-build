(* E40U - For noForth C&V5994 lp.0, hardware SPI on MSP430G5994.
   SPI i/o with two Launchpad boards and/or Egel kits

  Connect the SPI lines of USCI-B0 P1.5, P1.6 & P1.7 to same pins on the
  other board. Connect 6 leds to P2 and start the slave on the unit with 
  the led board. More info on page 445 of SLAU144J.PDF Configuration of 
  the pins in page 49 of SLAS735J.PDF
  User words are: SPI-MASTER  SPI-SLAVE1  SPI-SLAVE2

  SPI master & slave

                     MSP430G5994
                  -----------------
              /|\|              XIN|-
               | |                 |
               --|RST          XOUT|-
                 |                 |
                 |             P5.0|-> Data Out (UCB1SIMO)
                 |                 |
           LED <-|P1.0         P5.1|<- Data In (UCB1SOMI)
                 |                 |
           Out <-|P1.4         P5.2|-> Serial Clock Out (UCB1CLK)

Used register adresses:
 0020 = P1IN      - Input register
 0021 = P1OUT     - Output register
 0022 = P1DIR     - Direction register
 0026 = P1SEL     - 0C0
 0027 = P1REN     - Resistance on/off
 0041 = P1SEL2    - 0C0
 0029 = P2OUT     - Output register
 002A = P2DIR     - Direction register
 002E = P2SEL     - Configuration register 1
 002F = P2REN     - Resistance on/off
 0120 = WDTCL     - Off already
 0068 = UCB1CTL0  - 00F
 0069 = UCB1CTL1  - 081
 006A = UCB1BR0   - 0A0
 006B = UCB1BR1   - 000
 006C = UCB1CIE   - USCI interrupt enable
 006D = UCB1STAT  - USCI status
 006E = UCB1RXBUF - RX Data
 006F = UCB1TXBUF - TX Data
 0118 = UCB1I2C0A - NC
 011A = UCB1I2CSA - 042
 0001 = IE2       - 000
 0003 = IFG2      - 008 = TX ready, 004 = RX ready
  *)

