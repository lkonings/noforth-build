(* E40U - For noForth C&V5994 lp.0, hardware SPI on MSP430G5994.
   SPI i/o with two Launchpad boards and/or Egel kits

  Connect the SPI lines of USCI-B0 P1.5, P1.6 & P1.7 to same pins on the
  other board. Connect 6 leds to P2 and start the slave on the unit with 
  the led board. More info on page 445 of SLAU144J.PDF Configuration of 
  the pins in page 49 of SLAS735J.PDF
  User words are: SPI-MASTER  SPI-SLAVE1  SPI-SLAVE2

  SPI master & slave

                     MSP430G5994
                  -----------------
              /|\|              XIN|-
               | |                 |
               --|RST          XOUT|-
                 |                 |
                 |             P5.0|-> Data Out (UCB1SIMO)
                 |                 |
           LED <-|P1.0         P5.1|<- Data In (UCB1SOMI)
                 |                 |
           Out <-|P1.4         P5.2|-> Serial Clock Out (UCB1CLK)

Used register adresses:
 0020 = P1IN      - Input register
 0021 = P1OUT     - Output register
 0022 = P1DIR     - Direction register
 0026 = P1SEL     - 0C0
 0027 = P1REN     - Resistance on/off
 0041 = P1SEL2    - 0C0
 0029 = P2OUT     - Output register
 002A = P2DIR     - Direction register
 002E = P2SEL     - Configuration register 1
 002F = P2REN     - Resistance on/off
 0120 = WDTCL     - Off already
 0068 = UCB1CTL0  - 00F
 0069 = UCB1CTL1  - 081
 006A = UCB1BR0   - 0A0
 006B = UCB1BR1   - 000
 006C = UCB1CIE   - USCI interrupt enable
 006D = UCB1STAT  - USCI status
 006E = UCB1RXBUF - RX Data
 006F = UCB1TXBUF - TX Data
 0118 = UCB1I2C0A - NC
 011A = UCB1I2CSA - 042
 0001 = IE2       - 000
 0003 = IFG2      - 008 = TX ready, 004 = RX ready
  *)

202 constant __P1OUT_A
204 constant __P1DIR_A
222 constant __P3OUT_A
240 constant __P5IN_A
242 constant __P5OUT_A
244 constant __P5DIR_A
246 constant __P5REN_A
24A constant __P5SEL0_A
24C constant __P5SEL1_A
680 constant __UCB1CTLW0_A
68C constant __UCB1RXBUF_A
68E constant __UCB1TXBUF_A
6AC constant __UCB1IFG_A

extra\

hex
: MASTER-SETUP  ( -- )
    01 __UCB1CTLW0_A **bis                                                          \ UCB1CTLW0  Reset USCI
    bn 111 __P5SEL0_A *bis                                                          \ P5SEL0  P5.0=MOSI, P5.1=MISO, P5.2=CLK
    bn 111 __P5SEL1_A *bic                                                          \ P5SEL1  3-wire SPI
    bn 0100 __P5DIR_A *bis                                                          \ P5.2 Resistor on
    bn 0100 __P5REN_A *bis                                                          \ P5.2 pullup resistor
    6981 __UCB1CTLW0_A !                                                            \ UCB1CTLW0  Master using SMCLK
    08 __UCB1BRW_A !                                                                \ UCB1BRW  Clock is 16Mhz/8 = 2MHz
    00 __P5IE_A c!                                                                  \ UCB1P5IE_A Not used must be zero!
    01 __UCB1CTLW0_A **bic                                                          \ UCB1CTLW0  Free USCI
    bn 111 __P5OUT_A *bis                                                           \ P5OUT  P5.0=MOSI, P5.1=MISO, P5.2=CLK
__RED __P1DIR_A c!                                                                  \ P1DIR  P1.0=Red led
__RED __P1OUT_A c!                                                                  \ P1OUT  Red led off
    ;

: SLAVE-SETUP   ( -- )
    01 __UCB1CTLW0_A **bis                                                          \ UCB1CTLW0  Reset USCI
    bn 111 __P5SEL0_A *bis                                                          \ P5SEL0  P5.0=MOSI, P5.1=MISO, P5.2=CLK
    bn 111 __P5SEL1_A *bic                                                          \ P5SEL1  3-wire SPI
    bn 0100 __P5DIR_A *bis                                                          \ P5.2 Resistor on
    bn 0100 __P5REN_A *bis                                                          \ P5.2 pullup resistor
    6181 __UCB1CTLW0_A !                                                            \ UCB1CTLW0  Slave
    01 __UCB1CTLW0_A **bic                                                          \ UCB1CTLW0  Free USCI
    bn 100 __P5DIR_A *bic                                                           \ P5DIR  P5.2 = ingang
    begin  bn 100 __P5IN_A bit* until                                               \ P5IN  Master  in SPI mode (clock high)?
    ;

code SPI-I/O ( b1 -- b2 )                                                           \ Read and write at SPI-bus
    begin,  #2 __UCB1IFG_A & bit                                                    \ UCB1IFG
    cs? until,
    tos  __UCB1TXBUF_A & .b mov                                                     \ UCB1TXBUF
    begin,  #1 __UCB1IFG_A & bit                                                    \ UCB1IFG
    cs? until,
     __UCB1RXBUF_A &  tos .b mov                                                    \ UCB1RXBUF
    next
end-code

: >SPI      ( u -- )    spi-i/o drop ;
: SPI>      ( -- u )    0 spi-i/o ;


\ The SPI master send  the loop-index and receives & displays the answer
: SPI-MASTER    ( -- )
    master-setup
    begin
        100 0 do
            cr i u.  i spi-i/o u. 50 ms
            stop? if leave  then
        loop
    stop? until ;

: FLASH         ( -- )  __RED __P1OUT_A *bis  64 ms  __RED __P1OUT_A *bic  64 ms ;  \ P1OUT

\ The SPI slave receives data from the master puts it on the leds
\ and sends the same data back, this first example can be made
\ to do something usefull!!
: SPI-SLAVE1     ( -- )
    \ -1 02A c!  0 02E c!  flash                                                    \ P2DIR, P2SEL
    slave-setup  0 __UCB1TXBUF_A c!  0                                              \ UCB0TXBUF 
    begin
        \ spi-i/o dup 029 c!                                                        \ P2OUT
        \ bn 10 __P5OUT_A *bis                                                      \ P5OUT  P5.1=MISO
        spi-i/o dup .
    stop? until drop ;

: DEMO-SPI  ( u1 -- u2 )
    begin  01 __UCB1IFG_A bit* until  __UCB1RXBUF_A c@                              \ IFG2  RX?
    begin  02 __UCB1IFG_A bit* until  dup __UCB1TXBUF_A c! ;                        \ IFG2  TX?

\ This is the SPI slave demo from TI, it does nothing usefull however
: SPI-SLAVE2     ( -- )
    \ -1 02A c!  0 02E c!  flash                                                    \ P2DIR, P2SEL
    slave-setup  0 __UCB1TXBUF_A c!  0                                              \ UCB0TXBUF 
    begin
        \ demo-spi  dup 029 c!                                                      \ P2OUT
        spi-i/o dup .
    stop? until drop ;

: DEACTIVATE    ( -- )      
    bn 1000 __P3OUT_A *bis ;                                                        \ P3OUT  SPI off CSN high

\ The first written byte returns internal status always
\ It is saved in the value STATUS using SPI-COMMAND
: GET-STATUS    ( -- s )    bn 1000 __P3OUT_A *bic  FF spi-i/o deactivate ;         \ CSN
 
 shield SPI\  freeze

