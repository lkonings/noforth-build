hex
\ This key shows also the binary character pattern on the leds
: >LEDS         ( u -- )    __P3OUT_A c! ; \ P3OUT  Store pattern u at leds
: KEY*          ( c -- )    key)  dup >leds ;

: FLASH         ( -- )          \ Visualise startup
    FF >leds  64 ms             \ All leds on
    00 >leds  64 ms ;           \ All leds off

: SETUP         ( -- )
    \ Leds setup
    0 __P3SEL0_A c!             \ P3SEL  Port-2 all bits I/O
    0 __P3SEL1_A c!             \ P3SEL  Port-2 all bits I/O
    -1 __P3DIR_A c!             \ P3DIR  All bits of P3 are outputs

    01 __UCA3CTLW0_A **bis      \ UCA3CTLW0  Reset USCI

    \ UART-setup         
    3 __P6SEL0_A c!             \ P6SEL P6.0 P6.1
    1 __P6DIR_A c!              \ P6DIR 

    44 __UCA3MCTLW_A !          \ 
    A881 __UCA3CTLW0_A !        \ UCA3CTLW0  Master using SMCLK
    52 __UCA3BRW_A !            \ UCA3BRW  Clock is 16Mhz/52 = 2MHz

    01 __UCA3CTLW0_A **bic      \ UCA3CTLW0  Free USCI
;

: STARTUP       ( -- )
    setup  flash                \ Show new boot sequence
    ['] key* to 'key ;          \ Install new KEY

' startup to app  freeze

\ End ;;;
