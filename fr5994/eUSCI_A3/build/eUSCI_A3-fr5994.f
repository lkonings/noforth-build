(* E40U - For noForth C&V5994 lp.0, hardware SPI on MSP430G5994.
   SPI i/o with two Launchpad boards and/or Egel kits

  Connect the SPI lines of USCI-B0 P1.5, P1.6 & P1.7 to same pins on the
  other board. Connect 6 leds to P2 and start the slave on the unit with 
  the led board. More info on page 445 of SLAU144J.PDF Configuration of 
  the pins in page 49 of SLAS735J.PDF
  User words are: SPI-MASTER  SPI-SLAVE1  SPI-SLAVE2

  SPI master & slave

                     MSP430G5994
                  -----------------
              /|\|              XIN|-
               | |                 |
               --|RST          XOUT|-
                 |                 |
                 |             P5.0|-> Data Out (UCA3SIMO)
                 |                 |
           LED <-|P1.0         P5.1|<- Data In (UCA3SOMI)
                 |                 |
           Out <-|P1.4         P5.2|-> Serial Clock Out (UCA3CLK)

Used register adresses:
 0020 = P1IN      - Input register
 0021 = P1OUT     - Output register
 0022 = P1DIR     - Direction register
 0026 = P1SEL     - 0C0
 0027 = P1REN     - Resistance on/off
 0041 = P1SEL2    - 0C0
 0029 = P2OUT     - Output register
 002A = P2DIR     - Direction register
 002E = P2SEL     - Configuration register 1
 002F = P2REN     - Resistance on/off
 0120 = WDTCL     - Off already
 0068 = UCA3CTL0  - 00F
 0069 = UCA3CTL1  - 081
 006A = UCA3BR0   - 0A0
 006B = UCA3BR1   - 000
 006C = UCA3CIE   - USCI interrupt enable
 006D = UCA3STAT  - USCI status
 006E = UCA3RXBUF - RX Data
 006F = UCA3TXBUF - TX Data
 0118 = UCA3I2C0A - NC
 011A = UCA3I2CSA - 042
 0001 = IE2       - 000
 0003 = IFG2      - 008 = TX ready, 004 = RX ready
  *)

222 constant __P3OUT_A
241 constant __P6IN_A
243 constant __P6OUT_A
245 constant __P6DIR_A
247 constant __P6REN_A
24B constant __P6SEL0_A
24D constant __P6SEL1_A
620 constant __UCA3CTLW0_A
628 constant __UCA3MCTLW_A
62C constant __UCA3RXBUF_A
62E constant __UCA3TXBUF_A
63A constant __UCA3IFG_A
63C constant __UCA3IFG_A

extra\

hex
: MASTER-SETUP  ( -- )
    01 __UCA3CTLW0_A **bis                                                   \ UCA3CTLW0  Reset USCI
    bn 111 __P6SEL0_A *bis                                                   \ P6SEL0  P6.0=MOSI, P6.1=MISO, P6.2=CLK
    bn 111 __P6SEL1_A *bic                                                   \ P6SEL1  3-wire SPI
    bn 0100 __P6DIR_A *bis                                                   \ P6.2 Resistor on
    bn 0100 __P6REN_A *bis                                                   \ P6.2 pullup resistor
    0 __UCA3MCTLW_A !                                                        \ Not used but should be 0
    A981 __UCA3CTLW0_A !                                                     \ UCA3CTLW0  Master using SMCLK
    08 __UCA3BRW_A !                                                         \ UCA3BRW  Clock is 16Mhz/8 = 2MHz
    00 __P6IE_A c!                                                           \ UCA3P5IE_A Not used must be zero!
    01 __UCA3CTLW0_A **bic                                                   \ UCA3CTLW0  Free USCI
    bn 111 __P6OUT_A *bis                                                    \ P6OUT  P6.0=MOSI, P5.6=MISO, P6.2=CLK
    __LEDS_DIR
    __RED_OFF
    ;

: SLAVE-SETUP   ( -- )
    01 __UCA3CTLW0_A **bis                                                   \ UCA3CTLW0  Reset USCI
    bn 111 __P6SEL0_A *bis                                                   \ P6SEL0  P6.0=MOSI, P6.1=MISO, P6.2=CLK
    bn 111 __P6SEL1_A *bic                                                   \ P6SEL1  3-wire SPI
    bn 0100 __P6DIR_A *bis                                                   \ P6.2 Resistor on
    bn 0100 __P6REN_A *bis                                                   \ P6.2 pullup resistor
    0 __UCA3MCTLW_A !                                                        \ Not used but should be 0
    A181 __UCA3CTLW0_A !                                                     \ UCA3CTLW0  Slave
    01 __UCA3CTLW0_A **bic                                                   \ UCA3CTLW0  Free USCI
    bn 100 __P6DIR_A *bic                                                    \ P6DIR  P6.2 = ingang
    __LEDS_DIR
    begin  bn 100 __P6IN_A bit* until                                        \ P6IN  Master  in SPI mode (clock high)?
    ;

code SPI-I/O ( b1 -- b2 )                                                    \ Read and write at SPI-bus
    begin,  #2 __UCA3IFG_A & bit                                             \ UCA3IFG
    cs? until,
    tos  __UCA3TXBUF_A & .b mov                                              \ UCA3TXBUF
    begin,  #1 __UCA3IFG_A & bit                                             \ UCA3IFG
    cs? until,
     __UCA3RXBUF_A &  tos .b mov                                             \ UCA3RXBUF
    next
end-code

: >SPI      ( u -- )    spi-i/o drop ;
: SPI>      ( -- u )    0 spi-i/o ;

: FLASHER   ( n -- ) 0 do  __RED_ON  64 ms  __RED_OFF 64 ms loop ;

\ The SPI master send  the loop-index and receives & displays the answer
: SPI-MASTER    ( -- )
    master-setup
    begin
        100 0 do
            cr i u.  i spi-i/o u. 50 ms
            stop? if leave  then
        loop
    stop? until ;

\ The SPI slave receives data from the master puts it on the leds
\ and sends the same data back, this first example can be made
\ to do something usefull!!
: SPI-SLAVE1     ( -- )
    \ -1 02A c!  0 02E c!  flash                                             \ P2DIR, P2SEL
    slave-setup  0 __UCA3TXBUF_A c!  0                                       \ UCA3TXBUF 
    begin
        \ spi-i/o dup 029 c!                                                 \ P2OUT
        \ bn 10 __P6OUT_A *bis                                               \ P6OUT  P6.1=MISO
        spi-i/o dup .
    stop? until drop ;

\ ---- Probably less useful

: DEMO-SPI  ( u1 -- u2 )
    begin  01 __UCA3IFG_A bit* until  __UCA3RXBUF_A c@                       \ IFG2  RX?
    begin  02 __UCA3IFG_A bit* until  dup __UCA3TXBUF_A c! ;                 \ IFG2  TX?

\ This is the SPI slave demo from TI, it does nothing usefull however
: SPI-SLAVE2     ( -- )
    \ -1 02A c!  0 02E c!  flash                                             \ P2DIR, P2SEL
    slave-setup  0 __UCA3TXBUF_A c!  0                                       \ UCA3TXBUF 
    begin
        \ demo-spi  dup 029 c!                                               \ P2OUT
        spi-i/o dup .
    stop? until drop ;

\ ---- also freezed now

: DEACTIVATE    ( -- )      
    bn 1000 __P3OUT_A *bis ;                                                 \ P3OUT  SPI off CSN high

\ The first written byte returns internal status always
\ It is saved in the value STATUS using SPI-COMMAND
: GET-STATUS    ( -- s )    bn 1000 __P3OUT_A *bic  FF spi-i/o deactivate ;  \ CSN

shield SPI\  freeze
 
