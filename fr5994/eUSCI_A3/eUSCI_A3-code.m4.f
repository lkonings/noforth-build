extra\

hex
: MASTER-SETUP  ( -- )
m4_include(fr5994/snippets/UCA3-master-setup.m4.f)m4_dnl
    __LEDS_DIR
    __RED_OFF
    ;

: SLAVE-SETUP   ( -- )
    01 __UCA3CTLW0_A **bis   \ UCA3CTLW0  Reset USCI
    bn 111 __P6SEL0_A *bis   \ P6SEL0  P6.0=MOSI, P6.1=MISO, P6.2=CLK
    bn 111 __P6SEL1_A *bic   \ P6SEL1  3-wire SPI
    bn 0100 __P6DIR_A *bis   \ P6.2 Resistor on
    bn 0100 __P6REN_A *bis   \ P6.2 pullup resistor
    0 __UCA3MCTLW_A !        \ Not used but should be 0
    A181 __UCA3CTLW0_A !     \ UCA3CTLW0  Slave
    01 __UCA3CTLW0_A **bic   \ UCA3CTLW0  Free USCI
    bn 100 __P6DIR_A *bic    \ P6DIR  P6.2 = ingang
    __LEDS_DIR
    begin  bn 100 __P6IN_A bit* until   \ P6IN  Master  in SPI mode (clock high)?
    ;

m4_include(fr5994/snippets/UCA3-spi-io-word.m4.asm.f)m4_dnl

: >SPI      ( u -- )    spi-i/o drop ;
: SPI>      ( -- u )    0 spi-i/o ;

m4_include(fr5994/snippets/flasher.m4.f)m4_dnl

\ The SPI master send  the loop-index and receives & displays the answer
: SPI-MASTER    ( -- )
    master-setup
    begin
        100 0 do
            cr i u.  i spi-i/o u. 50 ms
            stop? if leave  then
        loop
    stop? until ;

\ The SPI slave receives data from the master puts it on the leds
\ and sends the same data back, this first example can be made
\ to do something usefull!!
: SPI-SLAVE1     ( -- )
    \ -1 02A c!  0 02E c!  flash  \ P2DIR, P2SEL
    slave-setup  0 __UCA3TXBUF_A c!  0    \ UCA3TXBUF 
    begin
        \ spi-i/o dup 029 c!      \ P2OUT
        \ bn 10 __P6OUT_A *bis   \ P6OUT  P6.1=MISO
        spi-i/o dup .
    stop? until drop ;

\ ---- Probably less useful

: DEMO-SPI  ( u1 -- u2 )
    begin  01 __UCA3IFG_A bit* until  __UCA3RXBUF_A c@        \ IFG2  RX?
    begin  02 __UCA3IFG_A bit* until  dup __UCA3TXBUF_A c! ;  \ IFG2  TX?

\ This is the SPI slave demo from TI, it does nothing usefull however
: SPI-SLAVE2     ( -- )
    \ -1 02A c!  0 02E c!  flash  \ P2DIR, P2SEL
    slave-setup  0 __UCA3TXBUF_A c!  0    \ UCA3TXBUF 
    begin
        \ demo-spi  dup 029 c!    \ P2OUT
        spi-i/o dup .
    stop? until drop ;

\ ---- also freezed now

: DEACTIVATE    ( -- )      
    bn 1000 __P3OUT_A *bis ;    \ P3OUT  SPI off CSN high

\ The first written byte returns internal status always
\ It is saved in the value STATUS using SPI-COMMAND
: GET-STATUS    ( -- s )    bn 1000 __P3OUT_A *bic  FF spi-i/o deactivate ; \ CSN

shield SPI\  freeze
 
