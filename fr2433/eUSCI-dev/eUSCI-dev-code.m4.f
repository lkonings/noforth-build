extra\

hex
: MASTER-SETUP  ( -- )
    01 __UCB0CTLW0_A **bis   \ UCB0CTLW0  Reset USCI
    __RED_DIR                \ __RED_C
    __RED_OFF                \ PxOUT  Red led off
    __UCB0_SEL0              \ PxSEL0  __UCB0_CLK_C, __UCB0_MOSI_C, __UCB0_MISO_C
    __UCB0_SEL1              \ PxSEL1  3-wire SPI
    __UCB0_DIR               \ __UCB0_CLK_C  Resistor on
    __UCB0_REN               \ __UCB0_CLK_C  pullup resistor
    6981 __UCB0CTLW0_A !     \ UCB0CTLW0  Master using SMCLK
    08 __UCB0BRW_A !         \ UCB0BRW  Clock is 16Mhz/8 = 2MHz
    01 __UCB0CTLW0_A **bic   \ UCB0CTLW0  Free USCI
    __UCB0_OUT               \ __UCB0_CLK_C, __UCB0_MOSI_C, __UCB0_MISO_C
    ;

: SLAVE-SETUP  ( -- )
    01 __UCB0CTLW0_A **bis   \ UCB0CTLW0  Reset USCI
    __RED_DIR                \ __RED_C
    __UCB0_SEL0              \ PxSEL0 __UCB0_CLK_C, __UCB0_MOSI_C, __UCB0_MISO_C
    __UCB0_SEL1              \ PxSEL1  3-wire SPI
    __UCB0_DIR               \ __UCB0_CLK_C  Resistor on
    __UCB0_REN               \ __UCB0_CLK_C  pullup resistor
    6181 __UCB0CTLW0_A !     \ UCB0CTLW0  Slave using SMCLK
    01 __UCB0CTLW0_A **bic   \ UCB0CTLW0  Free USCI
    __UCB0_OUT               \ __UCB0_CLK_C, __UCB0_MOSI_C, __UCB0_MISO_C
    __UCB0ClK_PN *bic        \ __UCB0_CLK_C is ingang
    begin  __UCB0ClK_PN bit* until   \ Master  in SPI mode (clock high)?
    ;

\ Master SPI routine
: SPI      ( u1 -- u2 )
    begin  02 __UCB0IFG_A bit* until  __UCB0TXBUF_A c!    \ IFG2  TX?
    begin  01 __UCB0IFG_A bit* until  __UCB0RXBUF_A c@ ;  \ IFG2  RX?

(*
code SPI ( b1 -- b2 )    \ Read and write at SPI-bus
    begin,  #2 __UCB0IFG_A & bit    \ UCB0IFG
    cs? until,
    tos  __UCB0TXBUF_A & .b mov       \ UCB0TXBUF
    begin,  #1 __UCB0IFG_A & bit    \ UCB0IFG
    cs? until,
     __UCB0RXBUF_A &  tos .b mov      \ UCB0RXBUF
    next
end-code
*)

: >SPI      ( u -- )    spi drop ;
: SPI>      ( -- u )    0 spi ;


\ The SPI master send  the loop-index and receives & displays the answer
: SPI-MASTER    ( -- )
    master-setup
    begin
        100 0 do
            cr i u.  i spi u. 50 ms
            stop? if leave  then
        loop
    stop? until ;

: FLASH         ( -- )
   __RED __P1OUT_A *bis  64 ms  __RED __P1OUT_A *bic  64 ms ;   \ P1OUT

\ The SPI slave receives data from the master puts it on the leds
\ and sends the same data back, this first example can be made
\ to do something usefull!!
: SPI-SLAVE1     ( -- )
    \ -1 02A c!  0 02E c!  flash  \ P2DIR, P2SEL
    slave-setup  0 __UCB0TXBUF_A c!  0    \ UCB0TXBUF 
    begin
        \ spi  dup 029 c!         \ P2OUT
        \ bn 10 PxOUT_A *bis     \ P5OUT  P5.1=MISO
        spi  dup .
    stop? until drop ;

: DEMO-SPI  ( u1 -- u2 )
    begin  01 __UCB0IFG_A bit* until  __UCB0RXBUF_A c@        \ IFG2  RX?
    begin  02 __UCB0IFG_A bit* until  dup __UCB0TXBUF_A c! ;  \ IFG2  TX?

\ This is the SPI slave demo from TI, it does nothing usefull however
: SPI-SLAVE2     ( -- )
    \ -1 02A c!  0 02E c!  flash  \ P2DIR, P2SEL
    slave-setup  0 __UCB0TXBUF_A c!  0    \ UCB0TXBUF 
    begin
        \ demo-spi  dup 029 c!    \ P2OUT
        spi  dup .
    stop? until drop ;

shield SPI\  freeze

