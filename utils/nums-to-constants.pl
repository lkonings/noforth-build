#!/usr/bin/perl
use strict;
use warnings;
use List::Util qw[min max];
use List::MoreUtils 'first_index'; 

my $target = shift;
my $project = shift;

# Files
my $codeFile = $target . '/' . $project . '/build/' . $project . '-code.m4.f';
my $constantsFile = $target . '/' . 'peripherals.f';
unless(-e $codeFile) {die "File $codeFile doesn't exist!"}; 
unless(-e $constantsFile) {die "File $codeFile doesn't exist!"}; 

#NOT WORKING:
##my $constantTail = `head -n 4 $constantsFile`;
##my $constantTail = system("sh", "tail", "-n 4 $constantsFile");
##head is an ELF file. Not a bash file!!
#WORKING:
#my $constantTail = system("head -n 4 $constantsFile");

#my $buildFile = $target . '/' . $project . '/' . 'build/xxx.f' ;
print "$codeFile\n";
print "$constantsFile\n";
#print "$buildFile\n";
#open(BUILDFILE, ">$buildFile") or die "Can't open $buildFile: $!";
#close(BUILDFILE); 

my $regex = '00,(\S*)';

my $m1 = '';
my $m2 = '';

$^I = '.bak';      # Enable in-place editing, backup in '.bak'.
   
# Create a filehandle called FILE and connect to the file.
open(FILE, $codeFile) or die "Can't open $codeFile: $!";
# Read the entire file into an array in memory.
my @lines = <FILE>;
close(FILE);

# Collect all nums that have to be replaced in an array.
my @nums;
foreach my $line (@lines) {
  #print $line;
  $_ = $line;
  while (/$regex/g) {
    push @nums, $1;
    #print "$1\n";
    #print "$1";
  }
}

open(CONSTANTSFILE, "$constantsFile") or die "Can't write to $codeFile: $!";
# Read the entire file into an array in memory.
my @constantLines = <CONSTANTSFILE>;
close(CONSTANTSFILE);

$regex = 'constant (__\S*)';

my $index;
my %hash;
my $num;
foreach (@nums) {
   $num = $_;
   #print "num: $num\n";
   $index = first_index { /^$num / } @constantLines;
   #print "index: $index\n";
   $constantLines[$index] =~ /$regex/gms;
   $hash{$num} = $1;
   print "$constantLines[$index] # $num\n";
}

open(FILE, ">$codeFile") or die "Can't write to $codeFile $!";
foreach my $line (@lines) {
  foreach my $key (keys %hash) {
    print "$key: $hash{$key}";
    #$line =~ s/(.*)0,$key(.*)/$1$hash{$key}$2/g;
    $line =~ s/00,$key/$hash{$key}/gms;
    #$_ = $line;
    #s/0,$key/$hash{$key}/g;
    print $line;
  }
  print FILE $line;
}
close(FILE);

