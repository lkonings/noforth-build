#!/usr/bin/perl
use strict;
use warnings;
use List::Util qw[min max];

# Files
my $codeFile = shift;  # Get the filename from command line.
my $peripheralsFile = shift;  # Get the filename from command line.
my $fconstantsFile = shift;  # Get the filename from command line.
#print $fconstantsFile;
open(FILE0, ">$fconstantsFile") or die "Can't create $fconstantsFile: $!";

my @m4s;

$^I = '.bak';      # Enable in-place editing, backup in '.bak'.
# Wait for key press to go on...
#<>;

# Create a filehandle called FILE1 and connect to the file.
open(FILE1, $codeFile) or die "Can't open $codeFile: $!";
# Read the entire file into an array in memory.
my @lines1 = <FILE1>;
foreach my $line1 (@lines1) {
  $_ = $line1;
  #print $line1;
  while (/(__.*?) /g) {
    push @m4s, $1;
    #print "$1\n";
  }
}
#foreach (@m4s) {
#  print "$_\n";
#}

#print "------------------------------------------\n";

my $regex = '(__.*)';
my $m1 = '';
# Walk through peripherals file to find constants,
# that are used in the code file.
# Write line to fconstants file if so.
# Create a filehandle called FILE2 and connect to the file.
open(FILE2, $peripheralsFile) or die "Can't open $peripheralsFile: $!";
# Read the entire file into an array in memory.
my @lines2 = <FILE2>;
foreach my $line2 (@lines2) {
  $_ = $line2;
  if (/$regex/g) {
    $m1 = $1;
    if (grep (/^$m1$/, @m4s)) {
      print FILE0  "$line2";
      #print "$1\n";
    }
  }
}

close(FILE2);
close(FILE1);
close(FILE0);
