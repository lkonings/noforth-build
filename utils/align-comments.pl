#!/usr/bin/perl
use strict;
use warnings;
use List::Util qw[min max];

# FileChange
my $filename = shift;  # Get the filename from command line.

my $regex = '(.*(\s\\\))(.*)';

my $m1 = '';
my $m2 = '';

$^I = '.bak';      # Enable in-place editing, backup in '.bak'.
# Wait for key press to go on...
#<>;
   
# Create a filehandle called FILE and connect to the file.
open(FILE, $filename) or die "Can't open $filename: $!";
# Read the entire file into an array in memory.
my @lines = <FILE>;
close(FILE);

my $max = 0;
open(FILE, ">$filename") or die "Can't write to $filename: $!";
foreach my $line (@lines) {
  $_ = $line;
  if (/(.*)\\\ /) {
    $m1 = $1;
    chop($m1);
    $m1 =~ s/\s+$//;
    $max = max($max, length($m1));
  }
}

foreach my $line (@lines) {
  $_ = $line;
  if (m/$regex/g) {
    $m1 = $1;
    $m2 = $3;
    chop($m1);
    $m1 =~ s/\s+$//;
    $m1 = $m1 .(' ' x ($max-length($m1)+2) );
    print FILE "$m1\\$m2\n";
  } else {
    print FILE $_;
  }
}
close(FILE);
