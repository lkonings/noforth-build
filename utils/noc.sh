#!/bin/bash
dos2unix -q $1
# Remove Forth comments with a \
perl -pi.bak -e 's/( \\.*)|(^\\.*)//g' $1
#./decomment.pl $1
# Remove Forth comments with (* *)
## perl -0pi -e 's/(?>\(\* )(?>.|\n)*(\*\))//gms' $1
#  perl -0pi -e 's/\(\*.*\*\)//gms' $1
# Remove tabs and spaces before the eol
perl -pi -e 's/([ \t]+)$//gm' $1
# Remove double blank lines
# sed -i 'N;/^\n$/d;P;D' $1
unix2dos -q $1
