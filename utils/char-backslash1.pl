#!/usr/bin/perl
use strict;
use warnings;
   
# FileChange
my $filename = shift;  # Get the filename from command line.

$^I = '.bak';  # Enable in-place editing, backup in '.bak'.
# Wait for key press to go on...
#<>;
   
# Create a filehandle called FILE and connect to the file.
open(FILE, $filename) or die "Can't open $filename: $!";
# Read the entire file into an array in memory.
my @lines = <FILE>;
close(FILE);

open(FILE, ">$filename") or die "Can't write to $filename: $!";
foreach my $line (@lines) {
  $_ = $line;
  s/(\s*)char\s+\\ /$1char_\\ /gi; 
  s/(\s*)\[char\]\s+\\ /$1\[char\]_\\ /gi; 
  s/(\s*)ch\s+\\ /$1ch_\\ /gi; 
  print FILE $_;
}
close(FILE);
