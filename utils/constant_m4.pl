#!/usr/bin/perl
use strict;
use warnings;
   
my %constants;

# FileChange
my $filename = shift;  # Get the filename from command line.

my $regex1 = '^(.*)\s(constant)\s(.*)$'; 
my $regex2 = '^(.*)\s(.*)\s(2constant)\s(.*)$'; 
my $regex3 = '^(.*)\s(xstring)\s(.*)$'; 

$^I = '.bak';      # Enable in-place editing, backup in '.bak'.
# Wait for key press to go on...
#<>;
   
# Create a filehandle called FILE and connect to the file.
open(FILE, $filename) or die "Can't open $filename: $!";
# Read the entire file into an array in memory.
my @lines = <FILE>;
close(FILE);

my $ok = 0;

open(FILE, ">$filename") or die "Can't write to $filename: $!";
foreach my $line (@lines) {
  $ok = 0;
  $_ = $line;
  if (s/$regex1/m4_define\($3,$1\)m4_dnl/gi) {
    #print "Matched substrings: $1, $2, $3 \n";  # print array
    $ok = 1;
  }
  if (s/$regex2/m4_define\($4,$1 $2\)m4_dnl/gi) {
    #print "Matched substrings: $1, $2, $3, $4 \n";  # print array
    $ok = 1;
  }
  if (s/$regex3/m4_define\($3,$1\)m4_dnl/gi) {
    #print "Matched substrings: $1, $2, $3 \n";  # print array
    $ok = 1;
  }
  if ($ok) {
    print FILE $_;
  }
}
close(FILE);
