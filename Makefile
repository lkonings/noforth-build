# Call this with either:
# - make make-target targetname projectname
# - make make-target targetname F=projectname
# - make
# Only make selects the first make-target.
# A make-target is the word before the colon(:).

nums:
	  cp TEST/test/test-code.m4.f TEST/test/build/
	  utils/nums-to-constants.pl TEST test
  
jawbreaker:
	  ./build longan jawbreaker
  
longan:
	  ./build longan $(F)
  
g2553:
	  ./build g2553 $(F)
  
fr2433:
	 ./build fr2433 $(F)

fr5994:
	 #./build fr5994 $(F)
	 ./build fr5994 eUSCI_A3

egelkit:
	 ./build egelkit $(F)
 
launchpad:
	 ./build launchpad $(F)

fr5739:
	./build fr5739 $(F)
 
g2et:
	./build g2et $(F)

