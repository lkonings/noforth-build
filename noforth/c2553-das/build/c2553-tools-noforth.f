

noforth\



hex
v: fresh inside
: STOP? ( -- true/false )
    key? dup 0= ?EXIT
    drop key  bl over =
    if drop begin key
    [ 2swap ]
    then hx 1B over = ?abort
    bl <>
    [ 2swap ]
; : STOPPER  ( - true/false ) [ 2swap ] again [ reveal 2drop

: .S ( -- )
    ?stack (.) space
    depth false
    ?do  depth i - 1- pick
        base @ 0A = if . else u. then
    loop ;


: >NFA   ( a -- nfa | 0 )
    dup origin
    chere within
    if  dup 1 and 0=
        if  1- dup c@ FF = +
            false
            begin over c@ 21 7F within
            while true /string 20 over <
            until
            then
            ?dup
            if  over c@
c:              3F and =
v:              7F and =
                if  dup 1 and ?EXIT
    then then then then
    drop false ;

: .NAME ( nfa -- ) count 1F and type ;

c: : IWORDS 40 ahead [ 2swap  2drop reveal
: WORDS   ( -- )
c:  false [ 2swap ] then  >r
    false >r
    fhere
    [ adr pfx-link hot - ] literal
    hot dup
    2over move drop
    bounds
    cr
    begin false dup
        2over
        do  dup i @ u<
            if  2drop i dup @
            then
        2 +loop
        dup stop? 0= and
    while
c:        dup 1+ c@ 40 and 2r@ drop =
v:        dup @voc vp c@ =
        if  r> 1+ >r
            dup lfa>n space .name space
            48 hor < if cr then

        then
c:      lnk@
v:      @
        swap !
    repeat
    2drop 2drop
c:    2r> (.) false .r drop
v:    r> (.) false .r
;


: pchar ( x -- )    dup hx 7F < and bl max ;

: DMP ( a -- )
   hx FF s>d du.str nip 1+
   swap    ( colw adr )
   begin cr base @ hex over 4 u.r ." : " base !
      swap ( adr colw )
      over 8 bounds do i c@ over .r loop ."  |"
      swap ( colw adr )
      8 false       do count pchar emit loop ." | "
   stopper until 2drop ;


: DECOM  ( a -- )
    cr dup 6 u.r space
    dup count pchar emit c@ pchar emit
    dup @ 6 u.r space
    dup >nfa ?dup
    if  ." --- "
v:      dup 1- c@ 7F and .voc
        dup .name
        c@ 80 < if ."  imm" then space
        @ cell- >nfa ?dup
        if  (.) space .name space
        then
        EXIT
    then

[ ' if dup @ u<
   ch Q over and
   swap invert ch ( and
   or beyond

( ] ( not for CC and VV )
    dup @
    dup 1 and
    if  dup -7800 <
        if          ch -  [ ' UNTIL >nfa ] literal
        else dup -7000 <
            if      ch +  [ ' IF >nfa ] literal
            else dup  7000 <
                if  2/ ." #" u. drop EXIT
                then
                dup  7800 <
                if  ch -  [ ' AGAIN >nfa ] literal
                else ch + [ ' AHEAD >nfa ] literal
                then
            then
        then
        .name space emit ch > emit space
        FFF and 7FF - + u. EXIT
    then
    drop
( Q ] ( continue for all versions )

    @
    dup >nfa ?dup
    if  .name drop EXIT
    then

    dup cell- >nfa ?dup
    if  (.) space .name space drop  EXIT
    then

    dup hot here within
    if  origin
        begin
            begin
                cell+ chere over u<
                if  2drop EXIT
                then
                2dup @ =
            until
            dup cell- >nfa ?dup
            if  .name ."   RAM location "
            then
        again
    then
    drop ;

: MSEE ( a -- )
    hx FFFE and
    begin dup decom cell+ stopper
    until drop ;
: SEE ( <name> -- )   ' msee ;

: MANY   ( -- )  >in @ stop? and >in ! ;

shield TOOLS\
v: fresh
freeze



