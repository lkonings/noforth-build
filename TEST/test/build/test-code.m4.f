code LPM3       D8 # sr bis  next  end-code \ Go from AM to LPM3

\ Activate VLOCLK as system clock for the MSP430 Current AM~0.030mA
\ Note: No baudrate is selected here!
: 12KHZ     ( -- )
    0000 __BCSCTL3_A c!    \ BCSCTL1   Switch low freq. mode on
    __P1IN_A __BCSCTL3_A c!    \ BCSCTL3   12kHz VLOCLK on
    __BCSCTL3_A 0058 c!    \ BCSCTL2   SMCLK & MCLK are VLOCLK
    0002 to ms# ;   \ MS timing

\ Current use of CPU without leds = 2,56mA
: 8MHZ      ( -- )  \ Baudrate not changed
    __BCSCTL3_A c@ __BCSCTL3_A c! \ BCSCTL1   set dco to 8 mhz
    10FC c@ 0056 c! \ DCOCTL
    0000 0058 c!    \ UCA0CTL2  DCO on
    07CF to ms# ;   \ MS timing

value TIME)  \ Decreases almost each second
\ Clock = 12KHz/8192 longest interval about 65535 sec.
\ Watchdog timer interrupt activated & I/O-port setup
: PREPARE     ( -- )     1 0 *bis  __RED 22 *bis  ; \ IE1, P2DIR
: WAKE        ( -- )     8mhz  5A94 120 ! ;  \ WDTCTL

: SLEEP       ( u -- ) \ Maximal time = 65535 sec = 1092 min = 18,2 hours
    to time)        \ Set sleep time
    12khz           \ Activate VLO 
    5A1D 120 !      \ Start WD/8192 as interval timer
    lpm3  wake ;    \ Sleep until time has passed

\ Decrease TIME) until it is zero
code TIMER      ( -- )
    #0 adr time) & cmp
    =? if,
        F8 # rp ) bic   \ Interrupt off, CPU active again!
        reti
    then,
    #-1 adr time) & add
    reti
end-code
' timer >body FFF4 vec! \ Install watchdog interrupt vector

: LED-ON    ( -- )      __RED  21 *bis ;
: LED-OFF   ( -- )      __RED  21 *bic ;
: FLASH     ( -- )      led-on FF ms  1 led-off FF ms ;

: MILL      ( n -- )    \ Funny windmill
    for
      ch / emit 80 ms  8 emit  led-on
      ch - emit 80 ms  8 emit  led-off
      ch \ emit 80 ms  8 emit  led-on
      ch | emit 80 ms  8 emit  led-off
      s? 0= key? or if  quit  then  \ Quit on S2 or keypress
    next ;

\ Flashing windmill with 5 seconds LMP3 intervals
\ Until S2 or a key is pressed while doing the windmill
: INTERVAL  ( -- )
    prepare  flash
    begin
        cr ." CPU Active "  5 mill  5 sleep
    again ;


' interval  to app
shield LPM3\  freeze

\ End ;;;
