(* Sleeping Launchpad, Egel kit or Micro Launchpad using the VLO & timer-A0

  Register addresses for Timer-A
160 - TA0CTL   Timer A control
162 - TA0CCTL0 Timer A Comp/Capt. control 0
172 - TACCR0   Timer A Compare register

  Used MSP430 power modes name shortcuts & short explanation:
  AM   = Active Mode      - CPU & clocks active
  LPM3 = Low Power Mode 3 - Only ACLK active

*)

